<?php
/*
Plugin Name: RUA Blog Subscriber
Plugin URI: https://hyperdrivedesigns.com
Description: WordPress plugin that displays a subscribe to blog form using a shortcode. You can place the shortcode in a widget, page or
post. This plugin also can display a list of email subscribers along with the ability to delete a subscriber and export the subscribers.
Author: Darren Ladner
Author URI: https://hyperdrivedesigns.com
Version: 2.7.4
*/

 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;
ob_start();
/*
|--------------------------------------------------------------------------
| CONSTANTS
|--------------------------------------------------------------------------
*/
// plugin folder url
if(!defined('RUA_PLUGIN_URL')) {
define('RUA_PLUGIN_URL', plugin_dir_url( __FILE__ ));
}
// plugin folder path
if(!defined('RUA_PLUGIN_DIR')) {
define('RUA_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
}
// plugin root file
if(!defined('RUA_PLUGIN_FILE')) {
define('RUA_PLUGIN_FILE', __FILE__);
}
// plugin version
if(!defined('RUA_VERSION')) {
define('RUA_VERSION', '2.7.4');
}

define( 'EDD_SL_STORE_URL', 'https://hyperdrivedesigns.com' );
define( 'EDD_SL_ITEM_RUA', 'RUA Blog Subscriber' );

/*
|--------------------------------------------------------------------------
| GLOBALS
|--------------------------------------------------------------------------
*/
global $rua_blog_subscriber_db_version;
$rua_blog_subscriber_db_version = "1.0";

/*
|--------------------------------------------------------------------------
| INCLUDES
|--------------------------------------------------------------------------
*/
require_once( RUA_PLUGIN_DIR.'includes/email-template.php' );
require_once( RUA_PLUGIN_DIR.'includes/new-page-templater.php' );
require_once( RUA_PLUGIN_DIR.'includes/install.php' );
require_once( RUA_PLUGIN_DIR.'includes/register-settings.php' );
require_once( RUA_PLUGIN_DIR.'includes/rua-tools.php' );
require_once( RUA_PLUGIN_DIR.'includes/rua-import.php' );
require_once( RUA_PLUGIN_DIR.'includes/rua-manual-subscriber.php' );
require_once( RUA_PLUGIN_DIR.'includes/rua-subscriber-admin.php' );
require_once( RUA_PLUGIN_DIR.'includes/rua-subscriber-settings.php' );
require_once( RUA_PLUGIN_DIR.'includes/rua-email-validation.php' );
require_once( RUA_PLUGIN_DIR.'includes/rua-save-subscriber.php' );
require_once( RUA_PLUGIN_DIR.'includes/rua-subscriber-form.php' );
require_once( RUA_PLUGIN_DIR.'includes/rua-email-members.php' );
require_once( RUA_PLUGIN_DIR.'includes/rua-email-admin.php' );
require_once( RUA_PLUGIN_DIR.'uninstall.php' );
require_once( RUA_PLUGIN_DIR.'includes/rua-php-captcha.php' );

add_action( 'plugins_loaded', array( 'RUAPageTemplater', 'get_instance' ) );

if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
// load our custom updater if it doesn't already exist
include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
}

// retrieve our license key from the DB
$license_key = trim( get_option( 'rua_blog_subscriber_license_key' ) );
// setup the updater
$edd_updater = new EDD_SL_Plugin_Updater( EDD_SL_STORE_URL, __FILE__, array(
'version' => '2.7.4', // current version number
'license' => $license_key, // license key (used get_option above to retrieve from DB)
'item_name' => EDD_SL_ITEM_RUA, // name of this plugin
'author' => 'Darren Ladner', // author of this plugin
'url' => home_url()
) );

function rua_blog_subscriber_license_menu() {
add_plugins_page( 'RUA License', 'RUA License', 'manage_options', 'rua-blog-subscriber-license', 'rua_blog_subscriber_license_page' );
}
add_action('admin_menu', 'rua_blog_subscriber_license_menu');

function rua_blog_subscriber_license_page() {
  $license = get_option( 'rua_blog_subscriber_license_key' );
  $status = get_option( 'rua_blog_subscriber_license_status' );
  ?>
  <div class="wrap">
    <h2><?php _e('RUA License Options'); ?></h2>
    <form method="post" action="options.php">
      <?php settings_fields('rua_blog_subscriber_license'); ?>
      <table class="form-table">
        <tbody>
          <tr valign="top">
            <th scope="row" valign="top">
              <?php _e('RUA License Key'); ?>
            </th>
            <td>
              <input id="rua_blog_subscriber_license_key" name="rua_blog_subscriber_license_key" type="text" class="regular-text" value="<?php esc_attr_e( $license ); ?>" />
              <label class="description" for="rua_blog_subscriber_license_key"><?php _e('Enter your RUA license key'); ?></label>
            </td>
          </tr>
          <?php if( false !== $license ) { ?>
            <tr valign="top">
              <th scope="row" valign="top">
                <?php _e('Activate RUA License'); ?>
              </th>
              <td>
                <?php if( $status !== false && $status == 'valid' )
                { ?>
                  <span style="color:green;"><?php _e('active'); ?></span>
                  <?php wp_nonce_field( 'rua_blog_subscriber_deactivate_nonce', 'rua_blog_subscriber_deactivate_nonce' ); ?>
                  <input type="submit" class="button-secondary" name="rua_blog_subscriber_license_deactivate" value="<?php _e('Deactivate License'); ?>"/>
                <?php
                }
                else
                {
                  wp_nonce_field( 'rua_blog_subscriber_license_nonce', 'rua_blog_subscriber_license_nonce' ); ?>
                  <input type="submit" class="button-secondary" name="rua_blog_subscriber_license_activate" value="<?php _e('Activate RUA License'); ?>"/>
                <?php
                }
                ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
      <?php submit_button(); ?>

    </form>
  <?php
}

function rua_blog_subscriber_register_option() {
  // creates our settings in the options table
  register_setting('rua_blog_subscriber_license', 'rua_blog_subscriber_license_key', 'rua_blog_subscriber_sanitize_license' );
}
add_action('admin_init', 'rua_blog_subscriber_register_option');

function rua_blog_subscriber_sanitize_license( $new ) {
  $old = get_option( 'rua_blog_subscriber_license_key' );
  if( $old && $old != $new ) {
    delete_option( 'rua_blog_subscriber_license_status' ); // new license has been entered, so must reactivate
  }
  return $new;
}

/************************************
* this illustrates how to activate
* a license key
*************************************/

function rua_blog_subscriber_activate_license() {
  // listen for our activate button to be clicked
  if( isset( $_POST['rua_blog_subscriber_license_activate'] ) ) {
    // run a quick security check
    if( ! check_admin_referer( 'rua_blog_subscriber_license_nonce', 'rua_blog_subscriber_license_nonce' ) )
      return; // get out if we didn't click the Activate button
    // retrieve the license from the database
    $license = trim( $_POST[ 'rua_blog_subscriber_license_key'] );

    // data to send in our API request
    $api_params = array(
      'edd_action'=> 'activate_license',
      'license'=> $license,
      'item_name' => urlencode( EDD_SL_ITEM_RUA ), // the name of our product in EDD,
      'url' => home_url()
    );

    // Call the custom API.
    $response = wp_remote_post( EDD_SL_STORE_URL, array(
      'timeout'=> 15,
      'sslverify' => false,
      'body' => $api_params
    ) );
    // make sure the response came back okay
    if ( is_wp_error( $response ) )
      return false;
    // decode the license data
    $license_data = json_decode( wp_remote_retrieve_body( $response ) );

    // $license_data->license will be either "active" or "inactive"
    update_option( 'rua_blog_subscriber_license_status', $license_data->license );
  }
}
add_action('admin_init', 'rua_blog_subscriber_activate_license');

/***********************************************
* Illustrates how to deactivate a license key.
* This will descrease the site count
***********************************************/

function rua_blog_subscriber_deactivate_license() {

  // listen for our activate button to be clicked
  if( isset( $_POST['rua_blog_subscriber_license_deactivate'] ) ) {

    // run a quick security check
    if( ! check_admin_referer( 'rua_blog_subscriber_deactivate_nonce', 'rua_blog_subscriber_deactivate_nonce' ) )
      return; // get out if we didn't click the Activate button

    // retrieve the license from the database
    $license = trim( get_option( 'rua_blog_subscriber_license_key' ) );

    // data to send in our API request
    $api_params = array(
      'edd_action'=> 'deactivate_license',
      'license' => $license,
      'item_name' => urlencode( EDD_SL_ITEM_RUA ), // the name of our product in EDD
      'url'=>home_url()
    );

    // Call the custom API.
    $response = wp_remote_post( EDD_SL_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );

    // make sure the response came back okay
    if ( is_wp_error( $response ) )
      return false;

    // decode the license data
    $license_data = json_decode( wp_remote_retrieve_body( $response ) );

    // $license_data->license will be either "deactivated" or "failed"
    if( $license_data->license == 'deactivated' )
      delete_option( 'rua_blog_subscriber_license_status' );
  }
}
add_action('admin_init', 'rua_blog_subscriber_deactivate_license');

/**
 * Enqueue scripts and styles.
 */
function rua_blog_public_subscriber_scripts() {
  wp_enqueue_style('rua-blog-subscriber-public-styles', plugins_url('css/rua-blog-subscriber-public-styles.css', __FILE__));
  wp_enqueue_script('rua', plugins_url( 'js/custom.js' , __FILE__ ) , array( 'jquery' ));
	wp_enqueue_style('rua-blog-subscriber-fonot-awesome', plugins_url('css/font-awesome.min.css', __FILE__));
  wp_localize_script( 'rua', 'MyAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php')));
	wp_enqueue_script('rua-js-validation', plugins_url( 'js/jquery.validate.min.js' , __FILE__ ));
}
add_action( 'wp_enqueue_scripts', 'rua_blog_public_subscriber_scripts' );

$rua_blog_subscriber_license_key_status = get_option('rua_blog_subscriber_license_status');

if ( $rua_blog_subscriber_license_key_status != 'valid' )
{
  function rua_activate_license_notice() {
    ?>
    <div class="notice notice-error is-dismissible">
        <p><?php _e( '<h5>RUA BLOG SUBSCRIBERS: Please activate your license in order for the settings page to show up and your plugin to work correctly.!</h5>
                    <p>Feel free to close the notice but until you enter your License key and activate it, the settings page for the plugin will not show up. Do not forget!
                    Also, you can just Deactivate the RUA Blog Subscriber plugin until you recieve your license to stop the admin notices.
                    You should have received your license in an email we sent. Look under the plugins menu and you should see a link to the license page.
                    If you did not recieve an email with your license or having trouble with your license getting actiavted, please send us a support ticket at
                    our website <a target="_blank" href="https://hyperdrivedesigns.com/js-support-ticket-controlpanel/">HERE</a></p>', 'rua-blog-subscriber' ); ?></p>
    </div>
    <?php
  }

  add_action( 'admin_notices', 'rua_activate_license_notice' );
}
else if ( $rua_blog_subscriber_license_key_status == 'valid' )
{
  add_action( 'admin_menu', 'rua_create_email_subscribers_menu' );
  function rua_create_email_subscribers_menu() {
    $menu = add_menu_page( 'Email Subscribers', 'Email Subscribers', 'manage_options', 'subscriber-admin.php', 'rua_create_subscriber_admin_page', 'dashicons-admin-users', 6 );
		$manual_menu = add_submenu_page( 'subscriber-admin.php', 'Add/Edit Subscribers', 'Add/Edit Subscribers', 'manage_options', 'manual-subscriber-admin.php', 'create_manual_subscriber_admin_page', 'dashicons-admin-users', 6 );
    $tools_menu = add_submenu_page( 'subscriber-admin.php', 'RUA Tools', 'RUA Tools', 'manage_options', 'rua-tools-admin.php', 'create_rua_tools_page', 'dashicons-admin-users', 6 );
    $import_menu = add_submenu_page( 'subscriber-admin.php', 'RUA Import', 'RUA Import', 'manage_options', 'rua-import-admin.php', 'create_rua_import_page', 'dashicons-arrow-up-alt', 6 );

    $submenu = add_options_page( 'RUA Settings', 'Email Subscribers Settings', 'manage_options', 'rua-subscribers-settings', 'rua_create_email_subscribers_settings_page' );

     add_action( 'admin_print_styles-' . $menu, 'rua_admin_custom_css' );
     add_action( 'admin_print_scripts-' . $menu, 'rua_admin_custom_js' );
		 add_action( 'admin_print_styles-' . $manual_menu, 'rua_admin_custom_css' );
     add_action( 'admin_print_scripts-' . $manual_menu, 'rua_admin_custom_js' );
     add_action( 'admin_print_styles-' . $tools_menu, 'rua_admin_custom_css' );
     add_action( 'admin_print_scripts-' . $tools_menu, 'rua_admin_custom_js' );
     add_action( 'admin_print_styles-' . $import_menu, 'rua_admin_custom_css' );
     add_action( 'admin_print_scripts-' . $import_menu, 'rua_admin_custom_js' );
     add_action( 'admin_print_styles-' . $submenu, 'rua_admin_custom_css' );
     add_action( 'admin_print_scripts-' . $submenu, 'rua_admin_custom_js' );
  }
}

// load bootstrap css for email subscriber admin page only
function rua_admin_custom_css()
{
	wp_enqueue_style('rua-font-awesome-css', plugins_url('css/font-awesome.min.css', __FILE__));
	wp_enqueue_style('rua-bootstrap-css', plugins_url('css/bootstrap.min.css', __FILE__));
	wp_enqueue_style('rua-datatables-bootstrap-css', plugins_url('css/dataTables.bootstrap.css', __FILE__));
	wp_enqueue_style('rua-blog-subscriber-admin-styles', plugins_url('css/rua-blog-subscriber-admin-styles.css', __FILE__));
	wp_enqueue_style( 'wp-color-picker' );
	wp_enqueue_style( 'jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css' );
	wp_enqueue_style( 'rua-google-fonts', 'https://fonts.googleapis.com/css?family=Raleway' );
  wp_enqueue_style( 'rua-admin-css', plugins_url('css/admin-css.css', __FILE__));
}

// load bootstrap ans custom js file for subscriber admin page only
function rua_admin_custom_js()
{
  wp_enqueue_script( 'jquery' );
  wp_enqueue_script( 'jquery-ui-datepicker' );
  wp_enqueue_script( 'wp-color-picker');
  wp_enqueue_script( 'rua-admin-bootstrap-js', plugins_url('js/bootstrap.min.js', __FILE__) );
  wp_enqueue_script( 'rua-blog-subscriber-custom-js', plugins_url( 'js/admin-custom.js', __FILE__ ), array( 'jquery', 'jquery-ui-datepicker') );
  wp_enqueue_script( 'rua-datatables-js', plugins_url( 'js/jquery.dataTables.min.js', __FILE__ ) );
  wp_enqueue_script( 'rua-datatables-bootstrap-js', plugins_url( 'js/dataTables.bootstrap.js', __FILE__ ) );
  wp_enqueue_script( 'rua-blog-subscriber-custom-datatables-js', plugins_url( 'js/custom-datatables.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
};

function rua_blog_subscriber_deactivation()
{
 flush_rewrite_rules();
}

register_deactivation_hook( RUA_PLUGIN_FILE, 'rua_blog_subscriber_deactivation' );
