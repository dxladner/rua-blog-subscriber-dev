// js datepicker for manual subscriber form
jQuery(document).ready(function() {
  jQuery('#ruasubdate').datepicker();
  jQuery('#rua_edit_subdate').datepicker();
});
// javascript code for deleting subscribers
jQuery(document).ready(function() {
  jQuery('.trash').click(function(){
    var sub_id = jQuery(this).attr('data-id');
    jQuery('#subscriber_id').val(sub_id);
    jQuery('#sub_id_holder').html(sub_id);
  });
});

// javascript for filter buttons
jQuery(document).ready(function() {
    jQuery('#all').click(function(event){event.preventDefault();
      jQuery('.subscribed').show();
      jQuery('.unsubscribed').show();
      jQuery('.unverified').show();
    });
    jQuery('#subscribed').click(function(event){event.preventDefault();
      jQuery('.subscribed').show();
      jQuery('.unsubscribed').hide();
      jQuery('.unverified').hide();
    });
    jQuery('#unsubscribed').click(function(event){event.preventDefault();
      jQuery('.unsubscribed').show();
      jQuery('.subscribed').hide();
      jQuery('.unverified').hide();
    });
    jQuery('#unverified').click(function(event){event.preventDefault();
      jQuery('.unverified').show();
      jQuery('.subscribed').hide();
      jQuery('.unsubscribed').hide();
    });
});