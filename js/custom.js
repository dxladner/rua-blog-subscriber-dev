
// javascript email check and subscriber save
jQuery(document).ready(function(){
    jQuery('#successMessage').hide();
    jQuery('.fa-spinner').hide();
    jQuery('#ruaCaptchaFailureMesage').hide();
    jQuery( "#ruaemail" ).focus(function(event){
      jQuery("#ruaValidationEmailMesage").html("");
      jQuery('#ruaSubmit').attr('disabled',false);
      jQuery( "#ruaemail" ).val('');
    });
    jQuery("#ruaSubmit").click(function(){
      jQuery('.fa-spinner').show();
      jQuery("#subscribeform").validate({
          rules: {
              ruaname: "required",
              ruaemail: {
                  required: true,
                  email: true
              },
          },
          messages: {
              ruaname: "Please enter your name",
              ruaemail: "Please enter a valid email address",
          },
                submitHandler: function(form)
                {
                  var email = jQuery("#ruaemail").val();
                  jQuery.ajax({
                      type: 'POST',
                      url: MyAjax.ajaxurl,
                      data: {
                        "action": "rua_email_validation",
                        "ruaemail":email,
                      },
                      success:function(data){
                      if(data==0){
                          var reg_nonce = jQuery('#rua_blog_subscriber_nonce').val();
                          var name = jQuery("#ruaname").val();
                          var email = jQuery("#ruaemail").val();
                          var phpcaptcha = jQuery("#phpcaptcha").val();
                          var status = jQuery("#ruasubstatus").val();
                          var siteid = jQuery("#ruasiteid").val();
                          var subdate = jQuery("#ruasubdate").val();
                            jQuery.ajax({
                                type: 'POST',
                                url: MyAjax.ajaxurl,
                                data: {
                                  "action": "rua_save_subscriber",
                                  "nonce": reg_nonce,
                                  "ruaname":name,
                                  "ruaemail":email,
                                  "ruaphpcaptcha":phpcaptcha,
                                  "ruasubstatus":status,
                                  "ruasiteid":siteid,
                                  "ruasubdate":subdate
                                },
                                success: function(data){
                                  if(data == 'FAIL')
                                  {
                                    jQuery('.fa-spinner').hide();
                                    jQuery('#ruaCaptchaFailureMesage').show();
                                    window.setTimeout(function(){location.reload()},5000);
                                  }
                                  else
                                  {
                                    jQuery('#ruaname').val('');
                                    jQuery('#ruaemail').val('');
                                    jQuery('#successMessage').show();
                                    jQuery('#subscribe_form').hide();
                                    setTimeout(function() {
                                      jQuery("#subscribeform")[0].reset();
                                      jQuery('#subscribe_form').show();
                                      jQuery('#successMessage').hide();
                                      jQuery('.fa-spinner').hide();
                                      jQuery("#ruaValidationEmailMesage").html("");
                                    }, 5000);
                                  }
                                }
                            });
                      }
                      else
                      {
                          jQuery("#ruaValidationEmailMesage").html("<span class='val_message'>You are already subscribed to this blog. Please check your email and click confirm to activate your subscription. Please wait as page reloads.</span>");
                          jQuery('#ruaSubmit').attr('disabled',true);
                          window.setTimeout(function(){location.reload()},2000);
                      }
                  }
              });
            }
        });
     });
  });
