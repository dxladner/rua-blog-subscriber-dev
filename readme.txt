=== RUA Blog Subscriber ===
Contributors: dxladner
Tags: blog subscription, blog subscribers, subscription, subscribe, multisite, subscriber dashboard, subscribe forms, published post notifcation emails
Donate link: https://hyperdrivedesigns.com/
Requires at least: 3.5
Tested up to: 4.7
Stable tag: trunk
License: GPLv2 or later
License URI:  http://www.gnu.org/licenses/gpl-2.0.html

Build your email list! Manage Subscribers from a Dashboard. Custom settings. Notify users of new posts. All without having users create accounts.

== Description ==
This multisite compatible plugin allows users to subscribe to your blog using a customized form. Users will have the ability to subscribe to your blog, verify their subscription and unsubscribe.
Your subscribers can be viewed from a dashboard in the admin section. You can see their subscription status, the data they unsubscribed and delete any subscribers. When a user
subscribes to your blog using the form a verification email is sent to their email address. In the email there is a Verify button that the subscriber will click that then
changes the subscribers status from Unverified to Subscribed. When you Publish a new post, all of your subscribers will receive an email announcing a new Post was Published with
a link in the email back to that post for their reading. Also inside all Published Posts emails, there is an Unsubscribed link. So if a subscriber wishes to Unsubscribe from
your blog, they can click the Unsubscribe link and their status will change from Subscribed to Unsubscribed and will no longer receive emails when you Publish a new Post.

Using the following shortcode [ruasubscriber], you can place the Subscription Form on a page or post. If you would like to place the shortcode in a widget you will need to use the Shortcode Widget Plugin. The Shortcode Widget plugin is a Free plugin from the WordPress Directory which allows you to place shortcodes in a widget. Out of the box, WordPress Widgets cannot handle shortcodes.

Get the <a href="https://wordpress.org/plugins/shortcode-widget/">Shortcode Widget Plugin here</a>.


== Installation ==
This section describes how to install the plugin and get it working.
1. Download and install the plugin from WordPress dashboard. You can also upload the entire “rua-blog-subscriber-lite” folder to the `/wp-content/plugins/` directory
2. Or from the Plugins page of your website, search for RUA Blog Subscriber and install from this page.
3. Activate the plugin through the ‘Plugins’ menu in WordPress
4. You should see a Link in the Admin Menu titled Email Subscriber Settings. Click here to go to the Email Subscribers Settings Page.
5. Here you would place all of your custom settings for the plugin.
6. You should also see a Link in the Admin Menu titled Email Subscribers. Click here to go to the Email subscribers Dashboard Page.
7. Use the following shortcode [ruasubscriber] to place the Subscription Form in your page, post or a text widget with the assistance of another plugin called Shortcode Widget. If you want to place the shortcode in a widget, you will need to use the Shortcode Widget plugin. The Shortcode Widget plugin is a Free plugin from the WordPress Directory which allows you to place Shortcodes in a widget. Out of the box, WordPress Widgets cannot handle shortcodes.
8. Get the <a href="https://wordpress.org/plugins/shortcode-widget/">Shortcode Widget Plugin here</a>.
9. For more Detailed Documentation, please visit our website <a href="http://ruablogsubscriber.com/rua-lite-documentation/">RUA Blog Subscriber Documentation</a>.

== Frequently Asked Questions ==
Has this plugin been tested

Yes. This plugin has been tested from WordPress version 3.5 - 4.7

For Frequently Asked Questions, please visit RUA Blog Subscriber website at <a href="http://ruablogsubscriber.com">RUA Blog Subscriber</a>.

What about support?

Create a support ticket at Hyperdrive Designs website: http://hyperdrivedesigns.com and I will take care of any issue. If you have any issues or have any questions, please
contact me and give me a chance to help you with your issue.

For complete information, visit the RUA Blog Subscriber website at <a href="http://ruablogsubscriber.com">RUA Blog Subscriber</a>.

== Changelog ==
1.0
Initial Commit

1.0.1
Made changes to readme.txt file

1.1.1
Made changes to the Subscriber Form that were causing issues with themes that used the Bootstrap CSS Framework.

1.2.1
Fixed the publish post email text.

1.2.2
Made changes to subscribe template to get the blog page url no matter what the name of the blog page is.

1.2.3
Added a Blog Page URL setting to help with the Subscribe Page template.

1.3.3
Added Two New Features. Manual Add/Edit Subscriber Form. Ability to chose a Custom Post Type for sending
published email.

1.3.4
Fixed some jQuery conflict issues.

2.3.4
Reorganized jQuery and CSS files. Reorganized file/folder structure. Added New Tools Page. Added CSV export. Added
new option to send email notifications based on a Post Category.

2.4.4
Created two new features. Added Email Admin upon new subscriber. Edited the Published Post feature where email
gets sent to subscriber on First Published Post ONLY. No more email sent on Edit Post.

2.5.4
Added new Site Information in Tools Page: Site ID. Added Site ID catch to form.

2.6.4
Added 'Join the Number of Other Subscribers' to Form. Updated the admin dashboard and settings styles. Corrected
typo on unsubscribe template.

2.7.4
Updated plugin page templater system to work with new WordPress 4.7 core version and older versions.

== Upgrade Notice ==
1.0
Initial Commit

1.0.1
Made changes to readme.txt file

1.1.1
Made changes to the Subscriber Form that were causing issues with themes that used the Bootstrap CSS Framework.

1.2.1
Fixed the publish post email text.

1.2.2
Made changes to subscribe template to get the blog page url no matter what the name of the blog page is.

1.2.3
Added a Blog Page URL setting to help with the Subscribe Page template.

1.3.3
Added Two New Features. Manual Add/Edit Subscriber Form. Ability to chose a Custom Post Type for sending
published email.

1.3.4
Fixed some jQuery conflict issues.

2.3.4
Reorganized jQuery and CSS files. Reorganized file/folder structure. Added New Tools Page. Added CSV export. Added
new option to send email notifications based on a Post Category.

2.4.4
Created two new features. Added Email Admin upon new subscriber. Edited the Published Post feature where email
gets sent to subscriber on First Published Post ONLY. No more email sent on Edit Post.

2.5.4
Added new Site Information in Tools Page: Site ID. Added Site ID catch to form.

2.6.4
Added 'Join the Number of Other Subscribers' to Form. Updated the admin dashboard and settings styles. Corrected
typo on unsubscribe template.

2.7.4
Updated plugin page templater system to work with new WordPress 4.7 core version and older versions.
