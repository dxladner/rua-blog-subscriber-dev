<?php
/**
 * RUA Tools Page
 *
*/

function create_rua_import_page() {

  if('POST' == $_SERVER['REQUEST_METHOD'])
  { // start POST
    // Verify nonce from ajax call
	if ( ! empty( $_POST ) && wp_verify_nonce( $_GET['nonce'], 'rua_import' ) )
	{
		echo '<br/>';
  		echo '<div class="wp-ui-notification">';
		echo __( 'Sorry, your nonce did not verify.', 'rua-blog-subscriber' );  			
		echo '</div>';
		exit;
	}
    else
    {
      	if( pathinfo( sanitize_file_name( $_FILES['csv_file']['name'] ),PATHINFO_EXTENSION ) !== "csv" ) 
      	{
      		echo '<br/>';
      		echo '<div class="wp-ui-notification">';
  			echo __( 'Only CSV files are allowed! Please refresh page and try again.', 'rua-blog-subscriber' );  			
  			echo '</div>';
      	    exit;
      	}
        // Set variables
        global $wpdb;
        $error_message = '';
        $success_message = '';
        $message_info_style = '';
        $getCSVFile = $_FILES['csv_file'];
        $getCSVFileName = sanitize_file_name( $_FILES['csv_file']['name'] );
        $getCSVFileExt = pathinfo( $getCSVFileName,PATHINFO_EXTENSION );
        $uploaddir = RUA_PLUGIN_DIR.'tmp_uploads/';
        $uploadfile = sanitize_file_name( $uploaddir.$getCSVFileName );

        // upload csv in tmp_uploads folder for fopen and reading later
        if ( move_uploaded_file( $_FILES['csv_file']['tmp_name'], $uploadfile ) )
        {
            // If the "Select Input File" input field is empty
            if( empty( $getCSVFile ) )
            {
                $error_message .= '* '.__( 'No Input File was selected. Please enter an Input File.','rua-blog-subscriber' ).'<br />';
            }

            // If all fields are input; and file is correct .csv format; continue
            if( !empty( $getCSVFile ) && ( $getCSVFileExt === 'csv' ) )
            {
                $db_cols = $wpdb->get_col( "DESC " . $_POST['table_select'], 0 );  // Array of db column names
        		// Remove first element of array (auto increment column)
        		unset($db_cols[0]);
                // Get the number of columns from the hidden input field (re-auto-populated via jquery)
                $numColumns = $_POST['num_cols'];

                // Open the .csv file and get it's contents
                $handle = fopen( $uploadfile, 'r' );
                if( $handle !== false )
                {
                    // Set variables
                    $values = array();
                    $too_many = '';  // Used to alert users if columns do not match

                    while( ( $row = fgetcsv( $handle )) !== false )
                    {  // Get file contents and set up row array
                      if( count( $row ) == $numColumns )
                      {  // If .csv column count matches db column count
                        $values[] = '("' . implode('", "', $row) . '")';  // Each new line of .csv file becomes an array
                      }
                    }

                    // If there are no rows in the .csv file AND the user DID NOT input more rows than available from the .csv file
                    if( empty( $values ) && ( $too_many !== 'true' ) )
                    {
                        $error_message .= '* '.__( 'Columns do not match.','rua-blog-subscriber' ).'<br />';
                        $error_message .= '* '.__( 'The number of columns in the database for this table does not match the number of columns attempting to be imported from the .csv file.','rua-blog-subscriber' ).'<br />';
                        $error_message .= '* '.__( 'Please verify the number of columns attempting to be imported".','rua-blog-subscriber' ).'<br />';
                    }
                    else
                    {
                        // If the user DID NOT input more rows than are available from the .csv file
                        if( $too_many !== 'true' )
                        {

                            $db_query_update = '';
                            $db_query_insert = '';

                            // Format $db_cols to a string
                            $db_cols_implode = implode(',', $db_cols);

                            // Format $values to a string
                            $values_implode = implode(',', $values);

                            $sql = 'INSERT INTO '.$_POST['table_select'] . ' (' . $db_cols_implode . ') ' . 'VALUES ' . $values_implode;
                            $db_query_insert = $wpdb->query($sql);
                        }

                          // If db db_query_update is successful
                          if ( $db_query_update )
                          {
                            $success_message = __( 'Congratulations!  The database has been updated successfully.','rua-blog-subscriber' );
                          }
                          // If db db_query_insert is successful
                          elseif ( $db_query_insert )
                          {
                            $success_message = __( 'Congratulations!  The database has been updated successfully.','rua-blog-subscriber' );
                            $success_message .= '<br /><strong>'.count($values).'</strong> '.__( 'record(s) were inserted into the', 'rua-blog-subscriber' ).' <strong>'.$_POST['table_select'].'</strong> '.__( 'database table.','rua-blog-subscriber' );
                          }
                          // If db db_query_insert is successful AND there were no rows to udpate
                          elseif( ( $db_query_update === 0 ) && ( $db_query_insert === '' ) )
                          {
                            $message_info_style .= '* '.__( 'There were no rows to update. All .csv values already exist in the database.','rua-blog-subscriber' ).'<br />';
                          }
                          else
                          {
                            $error_message .= '* '.__( 'There was a problem with the database query.','rua-blog-subscriber' ).'<br />';
                          }
                    }
                }
            }
            else
            {
              $error_message .= '* '.__( 'No valid .csv file was found. Please check the "Select Input File" field and ensure it points to a valid .csv file.','rua-blog-subscriber' ).'<br />';
            }

        }
        else
        {
          echo 'NO FILE UPLOADED';

        }
      } // end nonce
  } // end POST
  ?>
    <div class="wrap"><!-- start wrap -->
      <div class="container rua-container"><!-- start container -->
      <?php
      //If there is a message - info-style
  		if(!empty($message_info_style)) {
  			echo '<div class="wp-ui-primary">';
  			echo $message_info_style;
  			echo '<br />';
  			echo '</div>';
  		}

  		// If there is an error message
  		if(!empty($error_message)) {
  			echo '<div class="wp-ui-notification">';
  			echo $error_message;
  			echo '<br />';
  			echo '</div>';
  		}

  		// If there is a success message
  		if(!empty($success_message)) {
  			echo '<div class="wp-ui-highlight">';
  			echo $success_message;
  			echo '<br />';
  			echo '</div>';
  		}
      ?>
      <h3 class="text-center">Import Page</h3>
      <hr/>
        <div class="row">
          <form id="rua_import_form" method="post" class="form-inline" enctype="multipart/form-data" action="">
          <?php wp_nonce_field('rua_import','rua_import_nonce');?>
          <div class="col-md-10">
            <p>Please upload a CSV file. If you do not have a CSV file, there are plenty of online CSV generator tools you
            could use. This is an importer file tool and does NOT check for duplicate enteries already in the database. This
            tool only uploads and imports everything in your CSV file.</p>
            <label>Run Import</label>
            <input type="file" name="csv_file" id="csv_file" class="btn btn-primary btn-sm" />
            <input id="num_cols" name="num_cols" type="hidden" value="7" />
            <input id="num_cols_csv_file" name="num_cols_csv_file" type="hidden" value="" />
            <input id="table_select" name="table_select" type="hidden" value="wp_rua_blog_subscriber" />
            <br />
            <input id="importBtn" name="importBtn" type="submit" class="btn btn-success btn-sm" value="<?php _e('Import to DB', 'rua-blog-subscriber') ?>" />
			<i class="fa fa-download fa-lg" aria-hidden="true"></i>
          </div>
          </form>
        </div>

        <div class="row">
          <div class="col-md-10">
            <p><h3>IMPORTANT:</h3>
              This impoter tool does NOT check for duplicates. It only imports your csv file into the database table.
              Your csv file must match the rua-blog-subscriber database table columns in order to do a
              succesful import. There are 8 columns with the id being autoincrement. So each line in your
              csv file must be 7 values. If there is no value then you leave it empty but your line MUST
              contain 7 value spaces, empty or not. Below is a successful csv file example. </p>
            <img src="<?php echo RUA_PLUGIN_URL.'images/rua-csv-file.png'; ?>" />
          </div>
        </div>
      </div><!-- end container -->
    </div><!-- end wrap -->
    <?php
}
