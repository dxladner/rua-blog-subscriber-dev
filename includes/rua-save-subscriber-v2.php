<?php
/**
 * RUA Save Subscriber Page
 *
*/

add_filter( 'wp_mail_content_type', create_function( '', 'return "text/html";' ) );

if ( !function_exists('rua_save_subscriber' ) )
{
	function rua_save_subscriber()
	{
		// Verify nonce from ajax call
		if ( !isset( $_POST['nonce'] ) || !wp_verify_nonce( $_POST['nonce'], 'rua_blog_subscriber' ) )
		{
			print 'Sorry, your nonce did not verify.';
			exit;
		}
		else
		{
      $rua_turn_on_captcha = get_option( 'rua_turn_on_captcha' );
			if($rua_turn_on_captcha === 'on')
			{
						session_start();
						$phpsessioncaptcha = $_SESSION['captcha']['code'];
						$ruaphpcaptcha = $_POST['ruaphpcaptcha'];
						if ($phpsessioncaptcha === $ruaphpcaptcha)
						{

							$name = sanitize_text_field($_POST['ruaname']);
							$email = sanitize_email($_POST['ruaemail']);
							$status = sanitize_text_field($_POST['ruasubstatus']);
							$siteid = sanitize_text_field($_POST['ruasiteid']);
							$subdate = sanitize_text_field($_POST['ruasubdate']);
							$key = rand ( 100000, 999999 );
							global $wpdb;

							$wpdb->insert(
								'wp_rua_blog_subscriber',
								array(
									'subscriber_name' => $name,
									'subscriber_email' => $email,
									'subscriber_status' => $status,
									'site_id' => $siteid,
									'subscribe_date' => $subdate,
									'activation_key' => $key,
								),
								array(
									'%s',
									'%s',
									'%s',
									'%s',
									'%s',
									'%s',
								)
							);
							// if mailchimp plugin is activated
							if ( has_filter( 'rua_add_extentions_settings' ) )
							{
								require_once ABSPATH . '/wp-content/plugins/rua-mailchimp/includes/mailchimp-template.php';
							}
							// if aweber plugin is activated
							if ( has_filter( 'rua_add_aweber_extentions_settings' ) )
							{
								require_once ABSPATH . '/wp-content/plugins/rua-aweber/includes/aweber-template.php';
							}
							// if constant contact plugin is activated
							if ( has_filter( 'rua_add_constant_contact_extentions_settings' ) )
							{
								require_once ABSPATH . '/wp-content/plugins/rua-constant-contact/constant-contact-template.php';
							}

							 // send emails
							 rua_email_admin();
							 $site_name = get_option('rua_site_name');
							 $site_url = get_option('rua_site_url');
							 $site_email_logo = get_option('rua_email_logo');
							 $site_address = get_option('rua_company_address') .' '. get_option('rua_company_city') .' '.get_option('rua_company_zip') .' '. get_option('rua_company_state') .' '. get_option('rua_company_phone_number');
							 $from_email_address = get_option('rua_from_email_address');
						   $cc_email_address = get_option('rua_cc_email_address');
							 $subject = get_option('rua_email_subject');
							 $name = $name;
							 $email = $email;
							 $to = $email;

							$custom_email_enabled = get_option('rua_enable_custom_emails');
							$rua_default_email_footer = get_option('rua_default_email_footer');
							if($custom_email_enabled == 'enable')
							{
								$message = rua_get_email_body_header();
								$message .= '<p>Hi '.$name.',</p>';
								$message .= rua_get_email_body_content();
								$message .= '<p style="padding: 15px 0px; margin:0px;">
														 <a href="'.$site_url.'/subscribe/?key='.$key.'&email='.$email.'" style="background: #0061aa;padding: 10px 0px;-text-transform: uppercase;display: inline-block;color: #fff;font-size: 16px;text-decoration: none;width: 200px;text-align: center;">
														 confirm follow</a>
														 </p>';
									if($rua_default_email_footer == 'enable')
									{
										$message .= '<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
																&#169; '. $site_name .'
															</p>
															<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
																'.$site_address.'
															</p>
															<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
																<a href="'.$site_url.'">'.$site_name.'</a>
															</p>';
									}
								$message .= rua_get_email_body_footer();
							}
							else
							{
							 $message = '
													<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
													<html xmlns="http://www.w3.org/1999/xhtml">
													<head>
														<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
														<meta name="viewport" content="width=device-width"/>
													</head>
													<body style="width: 100% !important;min-width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0; color: #222222;
							font-family: \'Helvetica\', \'Arial\', sans-serif;font-weight: normal;text-align: left;line-height: 1.3;">
														<table style="border-spacing: 0; margin:0px auto;border-collapse: collapse; padding: 0;vertical-align: top;text-align: left;">
															<tr>
																<td style="word-break: break-word; -webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important; padding: 0;vertical-align: top;text-align: left;" align="center" valign="top">
																	<center style="width: 100%;">
																		<table style="width: 100%;margin: 0 auto;text-align: inherit; font-weight:500; max-width: 580px !important;">
																			<tr>
																				<td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important; vertical-align: top;border-bottom: 1px solid #e5e5e5; padding: 10px;text-align: center;" >
																					<img src="'.$site_email_logo.'" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: auto !important;height: auto !important; max-width: 100%;clear: both;" />
																				</td>
																			</tr>
																			<tr>
																				<td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important; padding: 0; vertical-align: top; text-align: left; border-bottom: 1px solid #e5e5e5;padding: 10px;text-align: left; padding: 45px; color: #676767; font-size: 14px; line-height: 20px;">
																					<p style="color: #676767;font-size: 16px;line-height: 25px; padding-bottom: 15px; margin:0px;">Hi '.$name.',
																					</p>
																					<p style="color: #676767;font-size: 16px;line-height: 25px; padding-bottom: 30px; margin:0px;">
																						You recently followed <a href="'.$site_url.'">'. $site_name .'</a> blog posts. This means you will receive each new post by email.
																						</p>
																					<p style="color: #676767; font-size: 16px; margin:0px; line-height: 25px;">
																						To activate your subscription, click confirm below. If you believe you received this email in error, ignore this message.
																					</p>
																					<p style="padding: 15px 0px; margin:0px;">
																					<a href="'.$site_url.'/subscribe/?key='.$key.'&email='.$email.'" style="background: #0061aa;padding: 10px 0px;text-transform: uppercase;display: inline-block;color: #fff;font-size: 16px;text-decoration: none;width: 200px;text-align: center;">
																					confirm follow</a>
																					</p>
																				</td>
																			</tr>
																			<tr>
																				<td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;text-align:center; padding:10px 0px;border-collapse: collapse !important; padding: 0;vertical-align: top;text-align: left;">
																					<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
																						&#169; '. $site_name .'
																					</p>
																					<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
																						'.$site_address.'
																					</p>
																					<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
																						<a href="'.$site_url.'">'.$site_name.'</a>
																					</p>
																				</td>
																			</tr>
																		</table>
																	</center>
																</td>
															</tr>
														</table>
													</body>
												</html>
											 ';
							}
							$headers = 'From: '.$site_name.' <'.$from_email_address.'>';
							wp_mail( $to, $subject, $message, $headers );
							die();
					}
					else
					{
						echo 'FAIL';
						die();
					}

			}
			// captcha turned off
			else if ($rua_turn_on_captcha === 'off' )
			{
					$name = sanitize_text_field($_POST['ruaname']);
					$email = sanitize_email($_POST['ruaemail']);
					$status = sanitize_text_field($_POST['ruasubstatus']);
					$siteid = sanitize_text_field($_POST['ruasiteid']);
					$subdate = sanitize_text_field($_POST['ruasubdate']);
					$key = rand ( 100000, 999999 );
					global $wpdb;

					$wpdb->insert(
						'wp_rua_blog_subscriber',
						array(
							'subscriber_name' => $name,
							'subscriber_email' => $email,
							'subscriber_status' => $status,
							'site_id' => $siteid,
							'subscribe_date' => $subdate,
							'activation_key' => $key,
						),
						array(
							'%s',
							'%s',
							'%s',
							'%s',
							'%s',
							'%s',
						)
					);
					// if mailchimp plugin is activated
					if ( has_filter( 'rua_add_extentions_settings' ) )
					{
						require_once ABSPATH . '/wp-content/plugins/rua-mailchimp/includes/mailchimp-template.php';
					}
					// if aweber plugin is activated
					if ( has_filter( 'rua_add_aweber_extentions_settings' ) )
					{
						require_once ABSPATH . '/wp-content/plugins/rua-aweber/includes/aweber-template.php';
					}
					// if constant contact plugin is activated
					if ( has_filter( 'rua_add_constant_contact_extentions_settings' ) )
					{
						require_once ABSPATH . '/wp-content/plugins/rua-constant-contact/constant-contact-template.php';
					}

					 // send emails
					 rua_email_admin();
					 $site_name = get_option('rua_site_name');
					 $site_url = get_option('rua_site_url');
					 $site_email_logo = get_option('rua_email_logo');
					 $site_address = get_option('rua_company_address') .' '. get_option('rua_company_city') .' '.get_option('rua_company_zip') .' '. get_option('rua_company_state') .' '. get_option('rua_company_phone_number');
					 $from_email_address = get_option('rua_from_email_address');
					 $cc_email_address = get_option('rua_cc_email_address');
					 $subject = get_option('rua_email_subject');
					 $name = $name;
					 $email = $email;
					 $to = $email;

					$custom_email_enabled = get_option('rua_enable_custom_emails');
					$rua_default_email_footer = get_option('rua_default_email_footer');
					if($custom_email_enabled == 'enable')
					{
						$message = rua_get_email_body_header();
						$message .= '<p>Hi '.$name.',</p>';
						$message .= rua_get_email_body_content();
						$message .= '<p style="padding: 15px 0px; margin:0px;">
												 <a href="'.$site_url.'/subscribe/?key='.$key.'&email='.$email.'" style="background: #0061aa;padding: 10px 0px;-text-transform: uppercase;display: inline-block;color: #fff;font-size: 16px;text-decoration: none;width: 200px;text-align: center;">
												 confirm follow</a>
												 </p>';
							if($rua_default_email_footer == 'enable')
							{
								$message .= '<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
														&#169; '. $site_name .'
													</p>
													<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
														'.$site_address.'
													</p>
													<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
														<a href="'.$site_url.'">'.$site_name.'</a>
													</p>';
							}
						$message .= rua_get_email_body_footer();
					}
					else
					{
					 $message = '
											<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
											<html xmlns="http://www.w3.org/1999/xhtml">
											<head>
												<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
												<meta name="viewport" content="width=device-width"/>
											</head>
											<body style="width: 100% !important;min-width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0; color: #222222;
					font-family: \'Helvetica\', \'Arial\', sans-serif;font-weight: normal;text-align: left;line-height: 1.3;">
												<table style="border-spacing: 0; margin:0px auto;border-collapse: collapse; padding: 0;vertical-align: top;text-align: left;">
													<tr>
														<td style="word-break: break-word; -webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important; padding: 0;vertical-align: top;text-align: left;" align="center" valign="top">
															<center style="width: 100%;">
																<table style="width: 100%;margin: 0 auto;text-align: inherit; font-weight:500; max-width: 580px !important;">
																	<tr>
																		<td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important; vertical-align: top;border-bottom: 1px solid #e5e5e5; padding: 10px;text-align: center;" >
																			<img src="'.$site_email_logo.'" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: auto !important;height: auto !important; max-width: 100%;clear: both;" />
																		</td>
																	</tr>
																	<tr>
																		<td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important; padding: 0; vertical-align: top; text-align: left; border-bottom: 1px solid #e5e5e5;padding: 10px;text-align: left; padding: 45px; color: #676767; font-size: 14px; line-height: 20px;">
																			<p style="color: #676767;font-size: 16px;line-height: 25px; padding-bottom: 15px; margin:0px;">Hi '.$name.',
																			</p>
																			<p style="color: #676767;font-size: 16px;line-height: 25px; padding-bottom: 30px; margin:0px;">
																				You recently followed <a href="'.$site_url.'">'. $site_name .'</a> blog posts. This means you will receive each new post by email.
																				</p>
																			<p style="color: #676767; font-size: 16px; margin:0px; line-height: 25px;">
																				To activate your subscription, click confirm below. If you believe you received this email in error, ignore this message.
																			</p>
																			<p style="padding: 15px 0px; margin:0px;">
																			<a href="'.$site_url.'/subscribe/?key='.$key.'&email='.$email.'" style="background: #0061aa;padding: 10px 0px;text-transform: uppercase;display: inline-block;color: #fff;font-size: 16px;text-decoration: none;width: 200px;text-align: center;">
																			confirm follow</a>
																			</p>
																		</td>
																	</tr>
																	<tr>
																		<td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;text-align:center; padding:10px 0px;border-collapse: collapse !important; padding: 0;vertical-align: top;text-align: left;">
																			<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
																				&#169; '. $site_name .'
																			</p>
																			<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
																				'.$site_address.'
																			</p>
																			<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
																				<a href="'.$site_url.'">'.$site_name.'</a>
																			</p>
																		</td>
																	</tr>
																</table>
															</center>
														</td>
													</tr>
												</table>
											</body>
										</html>
									 ';
					}
					$headers = 'From: '.$site_name.' <'.$from_email_address.'>';
					wp_mail( $to, $subject, $message, $headers );
					die();
			}
		}
	}
}

add_action( 'wp_ajax_rua_save_subscriber', 'rua_save_subscriber' );
add_action( 'wp_ajax_nopriv_rua_save_subscriber', 'rua_save_subscriber' );
