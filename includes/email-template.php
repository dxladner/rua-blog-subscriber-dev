<?php
/**
 * Email Template Functions
 *
 * @package     RUA Blog Subscriber
 * @subpackage  Email Template
 * @copyright   Copyright (c) 2016, Darren Ladner
 * @license     http://opensource.org/licenses/gpl-2.0.php GNU Public License
 * @since       1.0.0
*/

/**
 * Email Template Tags
 *
 * @access      private
 * @since       1.0.0 
 * @return      string
 *
 *  site_name
 *  site_url
 *  site_email_logo
 *  site_address
 *  from_email_address
 *  subject
 *
*/

function rua_email_template_tags($message) {
	
  $site_name = get_option('rua_site_name');
  $site_url = get_option('rua_site_url');
  $site_email_logo = "<img src=".get_option('rua_email_logo')." />";
  $site_address = get_option('rua_company_address') .' '. get_option('rua_company_city') .' '.get_option('rua_company_zip') .' '. get_option('rua_company_state') .' '. get_option('rua_company_phone_number');
  $from_email_address = get_option('rua_from_email_address');
  $subject = get_option('rua_email_subject');
  
	$message = str_replace('{site_name}', $site_name, $message);
  $message = str_replace('{site_url}', $site_url, $message);
  $message = str_replace('{site_email_logo}', $site_email_logo, $message);
  $message = str_replace('{site_address}', $site_address, $message);
  $message = str_replace('{from_email_address}', $from_email_address, $message);
  $message = str_replace('{subject}', $subject, $message);
 
  return $message;
}

/**
 * Email Template Header
 *
 * @access     private
 * @since      1.0.0
 * @echo      	string
*/

function rua_get_email_body_header() {
	ob_start(); ?>
	 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width"/>
      </head>
      <body style="width: 100% !important;	min-width: 100%;	-webkit-text-size-adjust: 100%;	-ms-text-size-adjust: 100%;	margin: 0;	padding: 0; color: #222222;
  font-family: \'Helvetica\', \'Arial\', sans-serif;	font-weight: normal; 	text-align: left;	line-height: 1.3;">
	<?php
	do_action('rua_email_body_header');
	return ob_get_clean();	
}

/**
 * Email Template Body for New Subscribers
 *
 * @access     private
 * @since      1.0.0
 * @echo      	string
*/


function rua_get_email_body_content() {
	$custom_email_message = get_option('rua_custom_email');
	$email_body = rua_email_template_tags($custom_email_message);
	return html_entity_decode($email_body);
}

/**
 * Email Template Footer for New Subscribers
 *
 * @access     private
 * @since      1.0.0
 * @return     string
*/

function rua_get_email_body_footer() {
	ob_start(); 
	do_action('rua_email_body_footer');	
	?>
    </body>
  </html>
	<?php
	return ob_get_clean();
}

/**
 * Email Template Body for Published Posts
 *
 * @access     private
 * @since      1.0.0
 * @echo      	string
*/


function rua_get_published_post_email_body_content() {
	$custom_published_post_email_message = get_option('rua_custom_published_post_email');
	$email_body = rua_email_template_tags($custom_published_post_email_message);
	return html_entity_decode($email_body);
}

/**
 * Email Template Footer for Published Posts
 *
 * @access     private
 * @since      1.0.0
 * @return     string
*/

function rua_get_published_post_email_body_footer() {
	ob_start(); 
	do_action('rua_published_post_email_body_footer');	
	?>
    </body>
  </html>
	<?php
	return ob_get_clean();
}
