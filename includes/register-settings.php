<?php
/**
 * Register Settings Page
 *
*/

if ( !function_exists('register_rua_settings' ) )
{
	function register_rua_settings() {

  if ( has_filter( 'rua_add_extentions_settings' ) )
	{
		add_option('rua_mailchimp_api_key', '');
  	add_option('rua_mailchimp_list_id', '');
	}
	if ( has_filter( 'rua_add_aweber_extentions_settings' ) )
	{
		add_option('rua_aweber_list_id', '');
	}
  if ( has_filter( 'rua_add_constant_contact_extentions_settings' ) )
	{
		add_option('rua_cc_username','');
		add_option('rua_cc_password','');
  	add_option('rua_cc_list_name','');
	}
  $default_site_name = get_bloginfo('name');
  add_option('rua_site_name', $default_site_name);
  $default_site_url = get_site_url();
  add_option('rua_site_url', $default_site_url);
	add_option('rua_site_contact_form', '');
	add_option('rua_show_form_subscribers', 'off');
  add_option('rua_company_address', '');
  add_option('rua_company_city', '');
  add_option('rua_company_state', '');
  add_option('rua_company_zip', '');
  add_option('rua_company_phone_number', '');
  add_option('rua_from_email_address', '');
	add_option('rua_form_header_size', 'h3');
  add_option('rua_email_subject', 'Blog Subscription');
  add_option('rua_enable_custom_emails', 'disabled');
	add_option('rua_default_email_footer', 'disabled');
	add_option('rua_custom_email', '');
  add_option('rua_email_logo', '');
  add_option('rua_form_header', 'SUBSCRIBE VIA EMAIL');
	add_option('rua_form_message', 'Enter your email address to subscribe to this blog and receive notifications of new posts by email.');
  add_option('rua_custom_message', 'An email was just sent to confirm your subscription.Please check your email and click confirm to activate your subscription.');
  add_option('rua_button_text', 'SUBMIT');
	add_option('rua_button_text_color', '#fff');
  $default_blog_url = get_site_url();
  add_option('rua_button_color', '#337ab7');
  add_option('rua_background_color', '#d3d3d3');
	add_option('rua_form_text_color', '#333');
	add_option('rua_enable_custom_published_post_email','disabled');
	add_option('rua_custom_published_post_email','');
	add_option('rua_default_published_post_email_footer');
  add_option('rua_enable_view_post_button');
  add_option( 'rua_blog_url' );
	add_option( 'rua_cpt' );
	add_option( 'rua_turn_off_all_posts', 'off' );
	add_option( 'rua_post_category' );
	add_option( 'rua_turn_on_captcha', 'off' );

 }
}
add_action( 'init', 'register_rua_settings' );
