<?php
/**
 * RUA Manual Subscriber Page
 *
*/

function create_manual_subscriber_admin_page() {
	?>
	<div class="wrap"><!-- start wrap -->
    <div class="container rua-container"><!-- start container -->
      <h3 class="text-center">Add Email Subscribers</h3>
      <hr/>
			<?php
					if (!empty($_POST['rua-add-submit']))
					{
						if ( ! empty( $_POST ) && check_admin_referer( 'rua_add_subscriber', 'rua_add_subscriber_nonce' ) )
						{
							$name = sanitize_text_field( $_POST['ruaname'] );
							$email = sanitize_email( $_POST['ruaemail'] );
							$status = sanitize_text_field( $_POST['ruasubstatus'] );
							$siteid = sanitize_text_field( $_POST['ruasiteid'] );
							$subdate = date('Y-m-d', strtotime( sanitize_text_field( $_POST['ruasubdate'] ) ) );

							global $wpdb;

							$wpdb->insert(
								'wp_rua_blog_subscriber',
								array(
									'subscriber_name' => $name,
									'subscriber_email' => $email,
									'subscriber_status' => $status,
									'site_id' => $siteid,
									'subscribe_date' => $subdate,
								),
								array(
									'%s',
									'%s',
									'%s',
									'%s',
									'%s',
								)
							);
							?>
							<div class="alert alert-success alert-dismissible" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
								<strong>Success!</strong> Subscriber Saved.
							</div>
							<?php
						}
					}
				
			?>
      <br/>
      <form id="rua_add_subscriber_form" name="rua_add_subscriber_form" class="form-horizontal" action="" method="post">
				<?php wp_nonce_field('rua_add_subscriber','rua_add_subscriber_nonce');?>
				<div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="ruaname">Name:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="ruaname" name="ruaname" value="" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="ruaemail">Email:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="ruaemail" name="ruaemail" value="" />
              </div>
            </div>
          </div>
      </div>
			<br>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-4">
							<label for="ruaname">Subscriber Status:</label>
						</div>
						<div class="col-md-8">
							<select class="form-control" id="ruasubstatus" name="ruasubstatus" value="">
								<option value="subscribed">Subscribed</option>
								<option value="unsubscribed">Unsubscribed</option>
								<option value="unverified">Unverified</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-4">
							<label for="ruasiteid">Site ID: (normally 1 unless Multi-site)</label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" id="ruasiteid" name="ruasiteid" value="1" />
						</div>
					</div>
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-4">
							<label for="ruasubdate">Subscribe Date:</label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" id="ruasubdate" name="ruasubdate" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-12 text-center">
            	<input name="rua-add-submit" type="submit" class="btn btn-hg btn-primary" value="<?php esc_attr_e('Save Subscriber', 'rua'); ?>" />
            </div>
					</div>
				</div>
			</div>
			</form>
			<br>
			<hr>
			<?php
			if (!empty($_POST['rua-pick-submit']))
			{
				if ( ! empty( $_POST ) && check_admin_referer( 'rua_pick_subscribe', 'rua_pick_subscribe_nonce' ) )
				{
					$subscriber_id = sanitize_text_field( $_POST['rua_pick_subscriber'] );
					global $wpdb;
					$pick_subscribers = $wpdb->get_results( "SELECT * FROM wp_rua_blog_subscriber WHERE id = '$subscriber_id'" );
				}
			}
			?>
			<!-- edit manual subscriber -->
		  <h3 class="text-center">Pick Email Subscribers</h3>
			<form id="rua_pick_subscriber_form" name="rua_pick_subscriber_form" class="form-horizontal" action="" method="post">
			<?php wp_nonce_field('rua_pick_subscribe','rua_pick_subscribe_nonce');?>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-4">
							<label for="rua_pick_subscriber">Pick Subscriber:</label>
						</div>
						<?php
						// get all subscriber for drop down list
						$site_id = get_current_blog_id();
						global $wpdb;
						$all_subscribers = $wpdb->get_results( "SELECT * FROM wp_rua_blog_subscriber WHERE site_id = '$site_id'" );
						?>
						<div class="col-md-8">
							<select class="form-control" id="rua_pick_subscriber" name="rua_pick_subscriber" value="">
								<?php
								foreach($all_subscribers as $subscriber)
								{
								?>
								<option value="<?php echo $subscriber->id; ?>"><?php echo $subscriber->subscriber_name; ?></option>
								<?php
								}
								?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-12 text-center">
            	<input name="rua-pick-submit" type="submit" class="btn btn-hg btn-primary" value="<?php esc_attr_e('Pick Subscriber', 'rua'); ?>" />
            </div>
					</div>
				</div>
			</div>
			</form>
			<div class="row">
				<div class="col-md-12 text-center">
					<div class="alert alert-info" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					Please Pick a Subscriber from the list above to show the Subscriber Edit Form.<br/>
					If you need to delete a Subscriber, you can do that from the Subscriber Dashboard.
				</div>
				</div>
			</div>
			<br>
			<?php
			if (!empty($_POST['rua-edit-submit']))
			{
				if ( ! empty( $_POST ) && check_admin_referer( 'rua_edit_subscriber', 'rua_edit_subscriber_nonce' ) )
				{
					$name = sanitize_text_field( $_POST['rua_edit_name'] );
					$email = sanitize_text_field( $_POST['rua_edit_email'] );
					$status = sanitize_text_field( $_POST['rua_edit_substatus'] );
					$siteid = sanitize_text_field( $_POST['rua_edit_siteid'] );
					$subdate = date('Y-m-d', strtotime( sanitize_text_field( $_POST['rua_edit_subdate'] ) ) );
					$id = sanitize_text_field( $_POST['rua_edit_id'] );
					global $wpdb;

					$wpdb->update(
						'wp_rua_blog_subscriber',
						array(
							'subscriber_name' => $name,
							'subscriber_email' => $email,
							'subscriber_status' => $status,
							'site_id' => $siteid,
							'subscribe_date' => $subdate,
						),
						array( 'ID' => $id ),
						array(
							'%s',
							'%s',
							'%s',
							'%s',
							'%s',
						)
					);
				?>
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<strong>Success!</strong> Subscriber Saved. Please wait page will refresh.
				</div>
			<?php
				header("Refresh:1");
				}
			}
			?>
			<h3 class="text-center">Edit Email Subscribers</h3>
			<form id="rua_edit_subscriber" name="rua_edit_subscriber" class="form-horizontal" action="" method="post">
			<?php wp_nonce_field('rua_edit_subscriber','rua_edit_subscriber_nonce');?>
				<?php
				if(!empty($pick_subscribers))
				{
					foreach($pick_subscribers as $pick_subscriber)
					{
					?>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<div class="col-md-4">
									<label for="rua_edit_name">Name:</label>
								</div>
								<div class="col-md-8">
									<input type="text" class="form-control" id="rua_edit_name" name="rua_edit_name" value="<?php echo $pick_subscriber->subscriber_name; ?>" />
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<div class="col-md-4">
									<label for="rua_edit_email">Email:</label>
								</div>
								<div class="col-md-8">
									<input type="text" class="form-control" id="rua_edit_email" name="rua_edit_email" value="<?php echo $pick_subscriber->subscriber_email; ?>" />
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
						<div class="form-group">
							<div class="col-md-4">
								<label for="rua_edit_substatus">Subscriber Status:</label>
							</div>
							<div class="col-md-8">
								<select class="form-control" id="rua_edit_substatus" name="rua_edit_substatus">
									<option value="<?php echo $pick_subscriber->subscriber_status; ?>"><?php echo $pick_subscriber->subscriber_status; ?></option>
									<option value="subscribed">Subscribed</option>
									<option value="unsubscribed">Unsubscribed</option>
									<option value="unverified">Unverified</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<div class="col-md-4">
								<label for="rua_edit_siteid">Site ID: (normally 1 unless Multi-site)</label>
							</div>
							<div class="col-md-8">
								<input type="text" class="form-control" id="rua_edit_siteid" name="rua_edit_siteid" value="<?php echo $pick_subscriber->site_id; ?>" />
							</div>
						</div>
					</div>
					</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<div class="col-md-4">
								<label for="rua_edit_subdate">Subscribe Date:</label>
							</div>
							<div class="col-md-8">
								<input type="text" class="form-control" id="rua_edit_subdate" name="rua_edit_subdate" value="<?php echo $pick_subscriber->subscribe_date; ?>" />
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<div class="col-md-4">
								<label for="rua_edit_id">Subscriber ID:</label>
							</div>
							<div class="col-md-8">
								<input type="text" name="rua_edit_id" id="rua_edit_id" readonly value="<?php echo $pick_subscriber->id; ?>" />
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="form-group">
							<div class="col-md-12 text-center">
								<input name="rua-edit-submit" type="submit" class="btn btn-hg btn-primary" value="<?php esc_attr_e('Save Subscriber', 'rua'); ?>" />
							</div>
						</div>
				</div>
				<?php
				}
			}
			?>
			</form>
		</div><!-- end container -->
	</div><!-- end wrap -->
	<?php
}
