<?php 
/**
 * RUA Email Admin 
 *
*/

if ( !function_exists( 'rua_email_admin' ) )
{
	function rua_email_admin()  {
		$site_name = get_option( 'rua_site_name' );
		$from_email_address = get_option( 'rua_from_email_address' );
		$admin_email = get_option( 'admin_email' );
		$subject = 'New Subscriber on '.$site_name.'';	
		$message = 'A vistor just subscribed to your blog!';
		$headers = 'From: '.$site_name.' <'.$from_email_address.'>';
		wp_mail( $admin_email, $subject, $message, $headers );
	}
}