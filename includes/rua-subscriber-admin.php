<?php
/**
 * RUA Subscriber Admin Page
 *
*/

function rua_create_subscriber_admin_page() {
 
  $site_id = get_current_blog_id();
  global $wpdb;
  $wpdb->get_results( "SELECT * FROM wp_rua_blog_subscriber WHERE subscriber_status = 'subscribed' AND site_id = '$site_id'" );
  $subscriber_count = $wpdb->num_rows;
  $all_subscribers = $wpdb->get_results( "SELECT * FROM wp_rua_blog_subscriber WHERE site_id = '$site_id'" );
	do_action('rua_admin_notices','');
  ?>
  <div class="wrap">
    <div class="container rua-container">
      <h3 class="text-center">Email Subscribers</h3>
      <hr/>
      <br/>
      <div class="row">
        <div class="col-md-3">
          Total <i class="fa fa-users fa-lg" aria-hidden="true"></i>: <?php echo $subscriber_count; ?>
       </div>
       <div class="col-md-5">
        <div class="btn-group">
           <a href="#" id="all" class="btn btn-primary btn-sm">
             <span class="glyphicon glyphicon-th-list">
             </span>
             All
           </a>
           <a href="#" id="subscribed" class="btn btn-default btn-sm">
             <span class="glyphicon glyphicon-th">
             </span>
             Subscribed
           </a>
           <a href="#" id="unsubscribed" class="btn btn-default btn-sm">
             <span class="glyphicon glyphicon-th">
             </span>
             Unsubscribed
           </a>
           <a href="#" id="unverified" class="btn btn-default btn-sm">
             <span class="glyphicon glyphicon-th">
             </span>
             Unverified
           </a>
         </div>
       </div>
       
       <div class="col-md-4">
         
       </div>
      </div>
			<br/>
			<div class="row">
        <div class="col-md-12">
					<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
    				<thead>
							 <th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>Subscribe Date</th>
								<th>Status</th>
								<th>Unsubscribe Date</th>
								<th>Delete</th>
             </thead>

						<tfoot>
							<tr>
								<th>ID</th>
								<th>Name</th>
								<th>Email</th>
								<th>Subscribe Date</th>
								<th>Status</th>
								<th>Unsubscribe Date</th>
								<th>Delete</th>
							</tr>
						</tfoot>

					<tbody>
							<?php
								foreach($all_subscribers as $subscriber) {
									 if($subscriber->subscriber_status == 'subscribed')
                      {
                        $data = 'subscribed';
                      }
                      elseif($subscriber->subscriber_status == 'unsubscribed')
                      {
                        $data = 'unsubscribed';
                      }
                      elseif($subscriber->subscriber_status == 'unverified')
                      {
                        $data = 'unverified';
                      }
									 		?>
                     	<tr class="<?php echo $data; ?>">
												<td><?php echo $subscriber->id; ?></td>
                    		<td><?php echo $subscriber->subscriber_name; ?></td>
                    		<td><?php echo $subscriber->subscriber_email; ?></td>
                    		<td><?php echo $subscriber->subscribe_date; ?></td>
                    		<td>
												<?php
                          if ( $subscriber->subscriber_status == 'subscribed')
                          {
                          ?>
                          <button type="button" class="btn btn-success btn-xs">
                            <?php echo $subscriber->subscriber_status; ?>
                          </button>
                          <?php
                          }
                          elseif ( $subscriber->subscriber_status == 'unsubscribed' )
                          {
                          ?>
                          <button type="button" class="btn btn-danger btn-xs">
                            Unsubscribed
                          </button>
                          <?php
                          }
                          elseif ( $subscriber->subscriber_status == 'unverified' )
                          {
                          ?>
                          <button type="button" class="btn btn-warning btn-xs">
                            Unverified
                          </button>
                          <?php
                          }
													?>
                     		</td>
												<td>
												<?php
                          if ( $subscriber->unsubscribe_date == '' )
                          {
                            echo '----';
                          }
                          else
                          {
                            echo $subscriber->unsubscribe_date;
                          }
												?>
                    		</td>
												<td>
                    			<a href="#delete" id="trash" data-id="<?php echo $subscriber->id; ?>" class="trash btn btn-danger btn-xs" role="button" data-toggle="modal" data-title="Delete" data-target="#delete">
                    				<span class="glyphicon glyphicon-trash"></span></i>
                    			</a>
                    		</td>
											</tr>
									<?php
									}
									?>
					</tbody>
				</table>
        </div><!-- end col-md-12 -->
      </div><!-- end row -->
    </div><!-- end container -->
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-header">
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
             <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
           </button>
            <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
         </div>
         <div class="modal-body">
           <?php
						if( !empty( $_POST['subscriber_id'] ) )
						{
							$id = intval( $_POST["subscriber_id"] );
							$retrieved_nonce = $_POST['_wpnonce'];
							if ( wp_verify_nonce( $retrieved_nonce, 'rua_delete_subscriber_nonce' ) )
							{
								global $wpdb;
								$wpdb->get_results( "DELETE FROM wp_rua_blog_subscriber WHERE id = '$id'" );
								echo '<script>location.reload();</script>';
							}
						}
           ?>
           <form id="delete-form" class="form-horizontal" method="post" action="">
						 <?php wp_nonce_field( 'rua_delete_subscriber_nonce' ); ?>
             <input type="hidden" value="subscriber_id" id="subscriber_id" name="subscriber_id" />
            <div class="alert alert-danger">
              <span class="glyphicon glyphicon-warning-sign"></span>
              Are you sure you want to delete this Record with ID of <span id="sub_id_holder"></span>
            </div>
         </div>
         <div class="modal-footer">
           <input id="modalDelete" type="submit" class="btn btn-success" value="Yes">
             <span class="glyphicon glyphicon-ok-sign"></span> 
           <input type="reset" class="btn btn-default" data-dismiss="modal" value="No">
             <span class="glyphicon glyphicon-remove"></span> 
         </form>
         </div><!-- end modal footer -->
       </div><!-- /.modal-content -->
     </div><!-- /.modal-dialog -->
   </div><!-- end modal fade -->
  </div><!-- end wrap -->
  <?php
}
