<?php
/**
 * RUA Subscriber Settings Page
 *
*/

function rua_create_email_subscribers_settings_page() {
  if('POST' == $_SERVER['REQUEST_METHOD'])
	{
		if ( has_filter( 'rua_add_extentions_settings' ) )
		{
			$rua_mailchimp_api_key = $_POST['rua_mailchimp_api_key'];
    	$rua_mailchimp_list_id = $_POST['rua_mailchimp_list_id'];
		}

		if ( has_filter( 'rua_add_aweber_extentions_settings' ) )
		{
			$rua_aweber_list_id = $_POST['rua_aweber_list_id'];
		}
		if ( has_filter( 'rua_add_constant_contact_extentions_settings' ) )
		{
			$rua_cc_list_name = $_POST['rua_cc_list_name'];
		}
    $rua_site_name = $_POST['rua_site_name'];
    $rua_site_url = $_POST['rua_site_url'];
		$rua_site_contact_form = $_POST['rua_site_contact_form'];
		$rua_show_form_subscribers = $_POST['rua_show_form_subscribers'];
    $rua_company_address = $_POST['rua_company_address'];
    $rua_company_city = $_POST['rua_company_city'];
    $rua_company_state = $_POST['rua_company_state'];
    $rua_company_zip = $_POST['rua_company_zip'];
    $rua_company_phone_number = $_POST['rua_company_phone_number'];
    $rua_from_email_address = $_POST['rua_from_email_address'];
		$rua_form_header_size = $_POST['rua_form_header_size'];
    $rua_email_subject = $_POST['rua_email_subject'];
    $rua_enable_custom_emails = $_POST['rua_enable_custom_emails'];
		$rua_default_email_footer = $_POST['rua_default_email_footer'];
    $rua_custom_email = $_POST['rua_custom_email'];
    $rua_email_logo = $_POST['rua_email_logo'];
    $rua_form_header = $_POST['rua_form_header'];
		$rua_form_message = $_POST['rua_form_message'];
    $rua_custom_message = $_POST['rua_custom_message'];
    $rua_button_text = $_POST['rua_button_text'];
		$rua_button_text_color = $_POST['rua_button_text_color'];
    $rua_button_color = $_POST['rua_button_color'];
    $rua_background_color = $_POST['rua_background_color'];
		$rua_form_text_color = $_POST['rua_form_text_color'];
		$rua_enable_custom_published_post_email = $_POST['rua_enable_custom_published_post_email'];
		$rua_custom_published_post_email = $_POST['rua_custom_published_post_email'];
		$rua_default_published_post_email_footer = $_POST['rua_default_published_post_email_footer'];
		$rua_enable_view_post_button = $_POST['rua_enable_view_post_button'];
    $rua_blog_url = sanitize_text_field( $_POST['rua_blog_url'] );
		$rua_cpt = sanitize_text_field( $_POST['rua_cpt'] );
		$rua_turn_off_all_posts = sanitize_text_field( $_POST['rua_turn_off_all_posts'] );
		$rua_post_category = sanitize_text_field( $_POST['rua_post_category'] );
    $rua_turn_on_captcha = sanitize_text_field( $_POST['rua_turn_on_captcha'] );


   	if ( has_filter( 'rua_add_extentions_settings' ) )
		{
			update_option('rua_mailchimp_api_key', $rua_mailchimp_api_key);
    	update_option('rua_mailchimp_list_id', $rua_mailchimp_list_id);
		}
		if ( has_filter( 'rua_add_aweber_extentions_settings' ) )
		{
			update_option('rua_aweber_list_id', $rua_aweber_list_id);
		}
    if ( has_filter( 'rua_add_constant_contact_extentions_settings' ) )
		{
			update_option('rua_cc_list_name', $rua_cc_list_name);
		}
    update_option('rua_site_name', $rua_site_name);
    update_option('rua_site_url', $rua_site_url);
		update_option('rua_site_contact_form', $rua_site_contact_form);
		update_option('rua_show_form_subscribers', $rua_show_form_subscribers);
    update_option('rua_company_address', $rua_company_address);
    update_option('rua_company_city', $rua_company_city);
    update_option('rua_company_state', $rua_company_state);
    update_option('rua_company_zip', $rua_company_zip);
    update_option('rua_company_phone_number', $rua_company_phone_number);
    update_option('rua_from_email_address', $rua_from_email_address);
		update_option('rua_form_header_size', $rua_form_header_size);
    update_option('rua_email_subject', $rua_email_subject);
    update_option('rua_enable_custom_emails', $rua_enable_custom_emails);
		update_option('rua_default_email_footer', $rua_default_email_footer);
    update_option('rua_custom_email', $rua_custom_email);
    update_option('rua_email_logo', $rua_email_logo);
    update_option('rua_form_header', $rua_form_header);
		update_option('rua_form_message', $rua_form_message);
    update_option('rua_custom_message', $rua_custom_message);
    update_option('rua_button_text', $rua_button_text);
		update_option('rua_button_text_color', $rua_button_text_color);
    update_option('rua_button_color', $rua_button_color);
    update_option('rua_background_color', $rua_background_color);
		update_option('rua_form_text_color', $rua_form_text_color);
		update_option('rua_enable_custom_published_post_email', $rua_enable_custom_published_post_email);
		update_option('rua_custom_published_post_email', $rua_custom_published_post_email);
		update_option('rua_default_published_post_email_footer', $rua_default_published_post_email_footer);
		update_option('rua_enable_view_post_button', $rua_enable_view_post_button);
    update_option( 'rua_blog_url', $rua_blog_url );
		update_option( 'rua_cpt', $rua_cpt );
		update_option( 'rua_turn_off_all_posts', $rua_turn_off_all_posts );
		update_option( 'rua_post_category', $rua_post_category );
    update_option( 'rua_turn_on_captcha', $rua_turn_on_captcha );
  }
  ?>
  <div class="wrap">
    <div class="container rua-container">
				<form id="es_settings" name="es_settings" class="form-horizontal" action="" method="post">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <h3 class="text-center"><?php _e( 'Email Subscribers Settings', 'rua' ); ?></h3>
              <p class="text-center"><?php _e( 'Use these options to set values for the emails that will be sent to subscribers.', 'rua' ); ?>
              </p>
            </div>
          </div>
        </div>
				<?php
          echo apply_filters('rua_add_extentions_settings', '');
					echo apply_filters('rua_add_aweber_extentions_settings', '');
					echo apply_filters('rua_add_constant_contact_extentions_settings', '');
        ?>
      <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_site_name">Name:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="rua_site_name" name="rua_site_name" value="<?php echo esc_attr( get_option('rua_site_name') ); ?>" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_site_url">Site Url:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="rua_site_url" name="rua_site_url" value="<?php echo esc_attr( get_option('rua_site_url') ); ?>" />
              </div>
            </div>
          </div>
      </div>
      <div class="row">
				  <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_company_address">Address:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="rua_company_address" name="rua_company_address" value="<?php echo esc_attr( get_option('rua_company_address') ); ?>" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_company_city">City:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="rua_company_city" name="rua_company_city" value="<?php echo esc_attr( get_option('rua_company_city') ); ?>" />
              </div>
            </div>
          </div>
			</div>
			<div class="row">
				  <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_company_state">State:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="rua_company_state" name="rua_company_state" value="<?php echo esc_attr( get_option('rua_company_state') ); ?>" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_company_zip">Zip:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="rua_company_zip" name="rua_company_zip" value="<?php echo esc_attr( get_option('rua_company_zip') ); ?>" />
              </div>
            </div>
          </div>
			</div>
			<div class="row">
				  <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_company_phone_number">Phone Number:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="rua_company_phone_number" name="rua_company_phone_number" value="<?php echo esc_attr( get_option('rua_company_phone_number') ); ?>" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_email_subject">From Email Subject:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="rua_email_subject" name="rua_email_subject" value="<?php echo esc_attr( get_option('rua_email_subject') ); ?>" />
              </div>
            </div>
          </div>
			</div>
			<div class="row">
				  <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_from_email_address">From Email Address:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="rua_from_email_address" name="rua_from_email_address" value="<?php echo esc_attr( get_option('rua_from_email_address') ); ?>" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_form_header_size">Form Header Size:</label>
              </div>
              <div class="col-md-8">
								<select class="form-control" id="rua_form_header_size" name="rua_form_header_size">
									<option value="<?php echo esc_attr( get_option('rua_form_header_size') ); ?>">
										<?php echo esc_attr( get_option('rua_form_header_size') ); ?>
									</option>
									<option value="h1">H1</option>
									<option value="h2">H2</option>
									<option value="h3">H3</option>
									<option value="h4">H4</option>
									<option value="h5">H5</option>
									<option value="h6">H6</option>
								</select>
              </div>
            </div>
          </div>
			</div>
			<div class="row">
				  <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_form_header">Form Header:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="rua_form_header" name="rua_form_header" value="<?php echo esc_attr( get_option('rua_form_header') ); ?>" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_button_text">Form Button Text:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control" id="rua_button_text" name="rua_button_text" value="<?php echo esc_attr( get_option('rua_button_text') ); ?>" />
              </div>
            </div>
          </div>
			</div>
			<div class="row">
				  <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_button_color">Form Button Color:</label>
              </div>
              <div class="col-md-8">
								<input type="text" class="form-control color-picker" id="rua_button_color" name="rua_button_color" value="<?php echo esc_attr( get_option('rua_button_color') ); ?>" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_button_text_color">Form Button Text Color:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control color-picker" id="rua_button_text_color" name="rua_button_text_color" value="<?php echo esc_attr( get_option('rua_button_text_color') ); ?>" />
              </div>
            </div>
          </div>
			</div>
			<div class="row">
				  <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_background_color">Form Background Color:</label>
              </div>
              <div class="col-md-8">
								<input type="text" class="form-control color-picker" id="rua_background_color" name="rua_background_color" value="<?php echo esc_attr( get_option('rua_background_color') ); ?>" />
							</div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_form_text_color">Form Text Color:</label>
              </div>
              <div class="col-md-8">
                <input type="text" class="form-control color-picker" id="rua_form_text_color" name="rua_form_text_color" value="<?php echo esc_attr( get_option('rua_form_text_color') ); ?>" />
              </div>
            </div>
          </div>
			</div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
						<div class="col-md-4">
                <label for="rua_custom_message">Custom Success Message:</label>
              </div>
              <div class="col-md-8">
								<textarea class="form-control" id="rua_custom_message" name="rua_custom_message" rows="2">
                  <?php echo esc_attr( get_option('rua_custom_message') ); ?>
                </textarea>
							</div>
          </div>
        </div>
				<div class="col-md-6">
          <div class="form-group">
              <div class="col-md-4">
                <label for="rua_form_message">Form Message:</label>
              </div>
              <div class="col-md-8">
                <textarea class="form-control" id="rua_form_message" name="rua_form_message" rows="3">
                  <?php echo esc_attr( get_option('rua_form_message') ); ?>
                </textarea>
              </div>
          </div>
        </div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-4">
							 <label for="rua_email_logo">Email Logo URL:</label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" id="rua_email_logo" name="rua_email_logo" value="<?php echo esc_attr( get_option('rua_email_logo') ); ?>" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<div class="col-md-4">
							<label for="rua_site_contact_form">Site Contact Form URL:</label>
						</div>
						<div class="col-md-8">
							<input type="text" class="form-control" id="rua_site_contact_form" name="rua_site_contact_form" value="<?php echo esc_attr( get_option('rua_site_contact_form') ); ?>" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
						<div class="form-group">
							  <div class="col-md-4">
									<label for="rua_blog_url"><?php _e( 'Your Blog Page URL:', 'rua' ); ?></label>
								</div>
							  <div class="col-md-8">
									<input type="text" class="form-control" id="rua_blog_url" name="rua_blog_url" value="<?php echo esc_attr( get_option( 'rua_blog_url' ) ); ?>" />
								</div>
						</div>
				</div>
				<div class="col-md-6">
						<?php
						$post_types = get_post_types();
						?>
						<div class="form-group">
							  <div class="col-md-4">
									<label for="rua_cpt"><?php _e( 'Custom Post Type:', 'rua' ); ?></label>
								</div>
							  <div class="col-md-8">
									<select id="rua_cpt" name="rua_cpt" class="form-control">

										<option value="<?php echo esc_attr( get_option( 'rua_cpt' ) ); ?>"><?php echo esc_attr( get_option( 'rua_cpt' ) ); ?></option>
										<option value="">Off</option>
										<?php
										foreach ( $post_types as $post_type )
										{
										?>
										<option value="<?php echo $post_type; ?>"><?php echo $post_type; ?></option>
										<?php
										}
										?>
									</select>
								</div>
						</div>
				</div>
			</div>
      <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-4">
                  <label for="rua_turn_off_all_posts"><?php _e( 'Turn Off All Posts: Off by default.', 'rua' ); ?></label>
                </div>
                <div class="col-md-8">
                  <select id="rua_turn_off_all_posts" name="rua_turn_off_all_posts" class="form-control">
										<option value="<?php echo get_option( 'rua_turn_off_all_posts' ); ?>"><?php echo get_option( 'rua_turn_off_all_posts' ); ?></option>
                  	<option value="off">Off</option>
										<option value="on">On</option>
                  </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
					 <?php
						$categories = get_categories( array(
								'orderby' => 'name',
								'parent' => 0
						) );
						?>
					<div class="form-group">
                <div class="col-md-4">
                  <label for="rua_post_category"><?php _e( 'Post by Category:', 'rua' ); ?></label>
                </div>
                <div class="col-md-8">
                  <select id="rua_post_category" name="rua_post_category" class="form-control">
										<?php $rua_category_id = get_option( 'rua_post_category' ); ?>
										<option value="<?php echo get_option( 'rua_post_category' ); ?>"><?php echo get_cat_name( $rua_category_id ); ?></option>
										<option value="">Off</option>
                  <?php
                    foreach ( $categories as $category )
                    {
                      ?>
                      <option value="<?php echo $category->term_id; ?>"><?php echo $category->name; ?></option>
                    <?php
                    }
                    ?>
                  </select>
                </div>
            </div>
        </div>
      </div>
			<div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_show_form_subscribers">Show # of Subscribers on Form:</label>
              </div>
              <div class="col-md-8">
                <select id="rua_show_form_subscribers" name="rua_show_form_subscribers" class="form-control">
                  <option value="<?php echo get_option( 'rua_show_form_subscribers' ); ?>"><?php echo get_option( 'rua_show_form_subscribers' ); ?></option>
                  <option value="off">Off</option>
                  <option value="on">On</option>
                </select>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <div class="col-md-4">
                <label for="rua_turn_on_captcha">Turn on Captcha</label>
              </div>
              <div class="col-md-8">
                <select id="rua_turn_on_captcha" name="rua_turn_on_captcha" class="form-control">
                  <option value="<?php echo get_option( 'rua_turn_on_captcha' ); ?>"><?php echo get_option( 'rua_turn_on_captcha' ); ?></option>
                  <option value="off">Off</option>
                  <option value="on">On</option>
                </select>
              </div>
            </div>
          </div>
      </div>
			<hr/>
			<div class="row">
				<div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Custom Emails</h3>
                    <span class="pull-right">
                        <!-- Tabs -->
                        <ul class="nav panel-tabs">
                            <li class="active"><a href="#tab1" data-toggle="tab">Custom Email for Blog Subscribers</a></li>
                            <li><a href="#tab2" data-toggle="tab">Custom Email for Published Post</a></li>
                        </ul>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
													<div class="row">
														<div class="col-md-12">
															<h5 class="text-center"><span class="fui-mail"></span> Custom Email for Blog Subscribers</h5>
															<div class="row">
																<div class="col-md-6">
																		<p>
																			You can use the default email or you can override it by using the
																			Custom Email Option. To use a Custom Email, set the Enable Custom
																			Email Dropdown list to Enable. By default, it is set to Disable.
																			Then use the Custom Email Editor to write your custom email. You
																			can use the Available template tags below the editor.
																		</p>
																		<p>
																			All Custom Emails will automatically include the users greeting and the
																			Confirm Follow Button. You can create the BODY of the message. If
																			you would like to include the default address footer then enable
																			this option in the Enable Default Email Footer dropdownlist.
																		</p>
																</div>
																<div class="col-md-6">
																	<p>Default Email sent to new Blog Subscribers</p>
																	<img class="text-align" src="<?php echo RUA_PLUGIN_URL; ?>images/rua-default-email.jpg" />
																</div>
															</div>
														</div>
													</div>
													<br/>
												<div class="row">
													<div class="col-md-6">
														 <label class="label-strong" for="rua_enable_custom_emails">Enable Custom Emails:</label>
													</div>
													<div class="col-md-6">
														 <label class="label-strong" for="rua_custom_email">Custom Email:</label>
													</div>
												</div>
												<div class="row">
													<div class="col-md-3">
														<div class="form-group">
																<select class="form-control" id="rua_enable_custom_emails" name="rua_enable_custom_emails" value="<?php echo esc_attr( get_option('rua_enable_custom_emails') ); ?>">
																	<option value="<?php echo get_option('rua_enable_custom_emails'); ?>"><?php echo get_option('rua_enable_custom_emails'); ?></option>
																	<option value="disable">Disable</option>
																	<option value="enable">Enable</option>
																</select>
																<br>
															 	<label class="label-strong" for="rua_default_email_footer">Enable Default Email Footer:</label>
																<select class="form-control" id="rua_default_email_footer" name="rua_default_email_footer" value="<?php echo esc_attr( get_option('rua_default_email_footer') ); ?>">
																	<option value="<?php echo get_option('rua_default_email_footer'); ?>"><?php echo get_option('rua_default_email_footer'); ?></option>
																	<option value="disable">Disable</option>
																	<option value="enable">Enable</option>
																</select>
														</div>
													</div>
													<div class="col-md-8">
														<div class="form-group">
																	 <?php
																			$content = esc_attr( get_option( 'rua_custom_email' ) );;
																			$editor_id = 'rua_custom_email';
																			$settings = array( 'media_buttons' => false,
																												'wpautop' => true,
																												'tinymce' => true,
																												'quicktags' => false);
																			wp_editor( $content, $editor_id, $settings );
																		?>
																	<br/>
																	<p>
																	Enter the email that is sent to subscribers after completing a successful purchase. Available template tags:<br/>
																	{site_name} - Your site name<br/>
																	{site_url} - Your site URL<br/>
																	{site_email_logo} - Your site Email Logo<br/>
																	{site_address} - Address of your Company/Website/Business<br/>
																	{from_email_address} - The FROM email address<br/>
																	{subject} - Subject of the Email sent to new blog subscribers<br/>
																	<br/>
																	In order to create paragraphs and line breaks, just use normal HTML formatting such as paragraph tag and line break tag.
																</p>
														</div>
													</div>
												</div>
											</div>
                        <div class="tab-pane" id="tab2">
													<div class="row">
														<div class="col-md-12">
															<h5 class="text-center"><span class="fui-mail"></span> Custom Email for Published Post</h5>
															<div class="row">
																<div class="col-md-6">
																		<p>
																			You can use the default published post email or you can override it by using the
																			Custom Email Option. To use a Custom Email, set the Enable Custom
																			Email Dropdown list to Enable. By default, it is set to Disable.
																			Then use the Custom Published Post Email Editor to write your custom email. You
																			can use the Available template tags below the editor.
																		</p>
																	  <p>
																			All Custom Published Post
																			Emails will automatically include the users greeting, the
																			Unsubscribed Link and the Subject will always be:
																			'New Blog Post on <em>YOUR SITE NAME</em>'.
  																		YOUR SITE NAME of course is your WordPress SITE NAME.
																			You can create the BODY of the message.
																		</p>
																		<p>
																			If you would like to include the default address footer then enable
																			this option in the Enable Default Email Footer dropdownlist. If you
																			would like to include the View Blog Post Button then enable this
																			option in the Enable View Post Button dropdownlist.
																	</p>
																</div>
																<div class="col-md-6">
																	<p>Default Email for Published Posts</p>
																	<img class="text-align" src="<?php echo RUA_PLUGIN_URL; ?>images/rua-default-pp-email.jpg" />
																</div>
															</div>
														</div>
													</div>
													<br/><br>
												<div class="row">
													<div class="col-md-6">
														 <label class="label-strong" for="rua_enable_custom_published_post_email">Enable Custom Email:</label>
													</div>
													<div class="col-md-6">
														 <label class="label-strong" for="rua_custom_published_post_email">Custom Published Post Email:</label>
													</div>
												</div>
												<div class="row">
													<div class="col-md-3">
														<div class="form-group">
																<select class="form-control" id="rua_enable_custom_published_post_email" name="rua_enable_custom_published_post_email" value="<?php echo esc_attr( get_option('rua_enable_custom_published_post_email') ); ?>">
																	<option value="<?php echo get_option('rua_enable_custom_published_post_email'); ?>"><?php echo get_option('rua_enable_custom_published_post_email'); ?></option>
																	<option value="disable">Disable</option>
																	<option value="enable">Enable</option>
																</select>
																<br>
															  <label class="label-strong" for="rua_default_published_post_email_footer">Enable Default Email Footer:</label>
																<select class="form-control" id="rua_default_published_post_email_footer" name="rua_default_published_post_email_footer" value="<?php echo esc_attr( get_option('rua_default_published_post_email_footer') ); ?>">
																	<option value="<?php echo get_option('rua_default_published_post_email_footer'); ?>"><?php echo get_option('rua_default_published_post_email_footer'); ?></option>
																	<option value="disable">Disable</option>
																	<option value="enable">Enable</option>
																</select>
																<br>
																<label class="label-strong" for="rua_enable_view_post_button">Enable View Post Button:</label>
																	<select class="form-control" id="rua_enable_view_post_button" name="rua_enable_view_post_button" value="<?php echo esc_attr( get_option('rua_enable_view_post_button') ); ?>">
																		<option value="<?php echo get_option('rua_enable_view_post_button'); ?>"><?php echo get_option('rua_enable_view_post_button'); ?></option>
																		<option value="disable">Disable</option>
																		<option value="enable">Enable</option>
																	</select>
															</div>
													</div>
													<div class="col-md-8">
														<div class="form-group">
																	 <?php
																			$content = esc_attr( get_option( 'rua_custom_published_post_email' ) );;
																			$editor_id = 'rua_custom_published_post_email';
																			$settings = array( 'media_buttons' => false,
																												'wpautop' => true,
																												'teeny' => true,
																												'quicktags' => false);
																			wp_editor( $content, $editor_id, $settings );
																		?>
																	<br/>
																	<p>
																	Enter the email that is sent to subscribers when a new post is published. Available template tags:<br/>
																	{site_name} - Your site name<br/>
																	{site_url} - Your site URL<br/>
																	{site_email_logo} - Your site Email Logo<br/>
																	{site_address} - Address of your Company/Website/Business<br/>
																	{from_email_address} - The FROM email address<br/>
																	<br/>
																	In order to create paragraphs and line breaks, just use normal HTML formatting such as paragraph tag and line break tag.
																</p>
														</div>
													</div>
												</div>
											</div>
                    </div>
                </div>
            </div>
        </div>
			</div>
      <hr/>
      <div class="row">
          <div class="col-md-8">
              <div class="form-group">
                <div class="col-md-3">
                  <label>Your Email Logo:</label>
                </div>
                 <div class="col-md-9">
                  <img src="<?php echo get_option('rua_email_logo'); ?>" />
                </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                <div class="col-md-12 text-center">
                  <input name="plugin_rua_options[submit]" type="submit" class="btn btn-hg btn-primary" value="<?php esc_attr_e('Save Settings', 'rua'); ?>" />
                </div>
              </div>
          </div>
      </div>
      </form>
    </div><!-- end container -->
  </div><!-- end wrap -->
  <?php
}
