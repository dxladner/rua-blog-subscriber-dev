<?php
/**
 * RUA Email Members Page
 *
*/

$rua_turn_off_all_posts = get_option( 'rua_turn_off_all_posts' );

if( $rua_turn_off_all_posts == 'off' )
{
	add_action('publish_post', 'rua_email_members');
}
else if ( $rua_turn_off_all_posts == 'on' )
{
	add_action( 'transition_post_status', 'rua_email_members_by_category', 10, 3 );
}

// email subscribers on CPT
$rua_cpt = get_option( 'rua_cpt' );
if ( !empty( $rua_cpt ) )
{
	add_action( 'publish_'.$rua_cpt, 'rua_email_members', 10, 2 );
}

function rua_excerpt_more( $more ) {
	return ' <br/>Click View Post Button To Read Full Post';
}
add_filter('excerpt_more', 'rua_excerpt_more');

function rua_get_the_excerpt( $post_id ) {
	global $post;  
  $save_post = $post;
  $post = get_post($post_id);
  setup_postdata( $post ); 
  $output = get_the_excerpt();
  $post = $save_post;
  return $output;
}

function rua_email_members($post_id) {
	if( get_post_meta($post_id, 'email_sent', 'true') != 'yes' ) 
	{
    $site_id = get_current_blog_id();
    $site_name = get_option('rua_site_name');
    $site_url = get_option('rua_site_url');
    $site_email_logo = get_option('rua_email_logo');
    $site_address = get_option('rua_company_address') .' '. get_option('rua_company_city') .' '. get_option('rua_company_zip') .' '. get_option('rua_company_state') .' '. get_option('rua_company_phone_number');
    $from_email_address = get_option('rua_from_email_address');
    $email_subject = get_option('rua_email_subject');
    $post_url = get_post_permalink( $post_id );
		$post_excerpt = rua_get_the_excerpt( $post_id );
		$post_featured_image =  wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'thumbnail' );
		$rua_enable_custom_published_post_email = get_option('rua_enable_custom_published_post_email');
		$rua_default_published_post_email_footer = get_option('rua_default_published_post_email_footer');
	  $rua_enable_view_post_button = get_option('rua_enable_view_post_button');
    global $wpdb;

    $subscribers_name_array = $wpdb->get_results( "SELECT subscriber_name, subscriber_email FROM wp_rua_blog_subscriber WHERE subscriber_status = 'subscribed' AND site_id = '$site_id'", ARRAY_A );

   	$subject = 'New Blog Post on '.$site_name.'';

    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );

    foreach($subscribers_name_array as $subscriber)
    {
			if($rua_enable_custom_published_post_email == 'enable')
			{
				$message = rua_get_email_body_header();
				$message .= '<p>Hi '.$subscriber['subscriber_name'].',</p>';
				$message .= rua_get_published_post_email_body_content();
				  if($rua_enable_view_post_button == 'enable')
					{
						$message .= '<p style="padding: 15px 0px; margin:0px; text-align:center;">
                          <a href="'.$site_url.'/blog/'.$post_url.'" class="confirm_follow" style="background: #0061aa;padding: 10px 0px;display: inline-block;color: #fff;font-size: 16px;text-decoration: none;width: 120px;text-align: center;">
                          	VIEW POST
                          </a>
													<p>'.$post_excerpt.'</p>
													<p>
													   <img src="' . $post_featured_image[0] . '" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: auto !important;height: auto !important; max-width: 100%;clear: both;" />
													</p>
                         </p>';
					}
				
					if($rua_default_published_post_email_footer == 'enable')
					{
						$message .= '<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
												&#169; '. $site_name .'
											</p>
											<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
												'.$site_address.'
											</p>
											<p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
												<a href="'.$site_url.'">'.$site_name.'</a>
											</p>';
					}
				$message .= ' <p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
                      	You are receiving this email because you opted in on our website. If you no longer want to receive these updates, 
												you may <a href="'.$site_url.'/unsubscribe/?email='.$subscriber['subscriber_email'].'">unsubscribe</a>.
                      </p>';
				$message .= rua_get_published_post_email_body_footer();
			}
			else
			{
      	$message = '
              <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
              <html xmlns="http://www.w3.org/1999/xhtml">
              <head>
                  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                  <meta name="viewport" content="width=device-width"/>
              </head>
              <body style="width: 100% !important;min-width: 100%;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0; color: #222222;
   font-family: \'Helvetica\', \'Arial\', sans-serif;font-weight: normal; text-align: left;line-height: 1.3;">
                <table style="border-spacing: 0; margin:0px auto;border-collapse: collapse; padding: 0;vertical-align: top;text-align: left;">
                  <tr>
                    <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important; padding: 0;vertical-align: top;text-align: left;" align="center" valign="top">
                      <center style="width: 100%;">
                          <table cellpadding="0" cellspacing="0" style="width: 100%;margin: 0 auto;text-align: inherit; font-weight:500; max-width: 580px !important;">
                            <tr>
                                <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important; vertical-align: top;border-bottom: 1px solid #e5e5e5; padding: 10px;text-align: center;" >
                                    <img src="'.$site_email_logo.'" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: auto !important;height: auto !important; max-width: 100%;clear: both;" />
                                </td>
                            </tr>
                            <tr>
                              <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;border-collapse: collapse !important; padding: 0; vertical-align: top; text-align: left; border-bottom: 1px solid #e5e5e5;padding: 10px;text-align: center; padding: 45px; color: #676767; font-size: 14px; line-height: 20px;">
                                <p style="color: #676767;font-size: 16px;line-height: 25px; padding-bottom: 15px; margin:0px; font-weight:bold;">
                                  '.$subscriber['subscriber_name'].', there is a new blog post on
                                   <a href="'.$site_url.'" style="color: #0d4fa0;text-decoration: none;">
                                   '.$site_name.'
                                   </a>
                                   </p>
                               <p style="margin:0px;">
                                  <img src="' . $image[0] . '" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: auto !important;height: auto !important; max-width: 100%;clear: both;" />
                               </p>
                               <p style="color: #676767;font-size: 16px; margin:0px;line-height: 25px; font-weight:bold; padding:10px 0px; color:#181818;">
                                  Welcome to the '.$site_name.' Blog!
                               </p>
                               <br>
                               <p style="padding: 15px 0px; margin:0px; text-align:center;">
                                  <a href="'.$site_url.'/blog/'.$post_url.'" class="confirm_follow" style="background: #0061aa;padding: 10px 0px;display: inline-block;color: #fff;font-size: 16px;text-decoration: none;width: 120px;text-align: center;">
                                    VIEW POST
                                  </a>
                               </p>
                              </td>
                            </tr>
                            <tr>
                                <td style="word-break: break-word;-webkit-hyphens: auto;-moz-hyphens: auto;hyphens: auto;text-align:center; padding:10px 0px;border-collapse: collapse !important; padding: 0;vertical-align: top;text-align: left;">
                                  <p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
                                    &#169; '. $site_name .'
                                  </p>
                                  <p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
                                    '.$site_address.'
                                  </p>
                                  <p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
                                    <a href="'.$site_url.'">'.$site_name.'</a>
                                  </p>
                                  <p style="color: #676767; margin:0px;font-size: 12px;line-height: 25px; text-align:center; ">
                                    You are receiving this email because you opted in on our website. If you no longer want to receive these updates, you may
                                    <a href="'.$site_url.'/unsubscribe/?email='.$subscriber['subscriber_email'].'">unsubscribe</a>.
                                  </p>
                                </td>
                            </tr>
                          </table>
                      </center>
                    </td>
                  </tr>
                </table>
              </body>
            </html>
          ';
				}
          $headers = 'From: '.$site_name.' <'.$from_email_address.'>';
          wp_mail( $subscriber['subscriber_email'], $subject, $message, $headers );
					add_post_meta( $post_id, 'email_sent', 'yes', true );
    }
	}
}

function rua_email_members_by_category( $new_status, $old_status, $post_id ) {
    if ( $old_status != 'publish' && $new_status == 'publish' )
    {
			$rua_post_category = get_option( 'rua_post_category' );
			
        // Post is published
        $category = array( $rua_post_category ); // Category ID, name or slug. You can use an array to pass more than one Category
        if ( !in_category ( $category, $post_id ) ) // If the post is not in your required category/s
          return;
        
				$site_id = get_current_blog_id();
				$site_name = get_option('rua_site_name');
				$site_url = get_option('rua_site_url');
				$site_email_logo = get_option('rua_email_logo');
				$site_address = get_option('rua_company_address') .' '. get_option('rua_company_city') .' '. get_option('rua_company_zip') .' '. get_option('rua_company_state') .' '. get_option('rua_company_phone_number');
				$from_email_address = get_option('rua_from_email_address');
				$email_subject = get_option('rua_email_subject');
    
				$post_url = get_post_permalink( $post_id );
				$rua_enable_custom_published_post_email = get_option('rua_enable_custom_published_post_email');
				$rua_default_published_post_email_footer = get_option('rua_default_published_post_email_footer');
				$rua_enable_view_post_button = get_option('rua_enable_view_post_button');
				global $wpdb;

				$subscribers_name_array = $wpdb->get_results( "SELECT subscriber_name, subscriber_email FROM wp_rua_blog_subscriber WHERE subscriber_status = 'subscribed' AND site_id = '$site_id'", ARRAY_A );

				$subject = 'New Blog Post on '.$site_name.'';

				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' );

			foreach($subscribers_name_array as $subscriber)
			{
				if($rua_enable_custom_published_post_email == 'enable')
				{
					$message = rua_get_email_body_header();
					$message .= '<p>Hi '.$subscriber['subscriber_name'].',</p>';
					$message .= rua_get_published_post_email_body_content();
						if($rua_enable_view_post_button == 'enable')
						{
							$message .= '<p style="padding: 15px 0px; margin:0px; text-align:center;">
														<a href="'.$site_url.'/blog/'.$post_url.'" class="confirm_follow" style="background: #0061aa;padding: 10px 0px;display: inline-block;color: #fff;font-size: 16px;text-decoration: none;width: 120px; text-align: center;">
															VIEW POST
														</a>
													 </p>';
						}

						if($rua_default_published_post_email_footer == 'enable')
						{
							$message .= '<p style="color: #676767; margin:0px; font-size: 12px; line-height: 25px; text-align:center; ">
													&#169; '. $site_name .'
												</p>
												<p style="color: #676767; margin:0px; font-size: 12px; line-height: 25px; text-align:center; ">
													'.$site_address.'
												</p>
												<p style="color: #676767; margin:0px; font-size: 12px; line-height: 25px; text-align:center; ">
													<a href="'.$site_url.'">'.$site_name.'</a>
												</p>';
						}
					$message .= ' <p style="color: #676767; margin:0px; font-size: 12px;line-height: 25px; text-align:center; ">
													You are receiving this email because you opted in on our website. If you no longer want to receive these updates, 
													you may <a href="'.$site_url.'/unsubscribe/?email='.$subscriber['subscriber_email'].'">unsubscribe</a>.
												</p>';
					$message .= rua_get_published_post_email_body_footer();
				}
				else
				{
					$message = '
								<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
								<html xmlns="http://www.w3.org/1999/xhtml">
								<head>
										<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
										<meta name="viewport" content="width=device-width"/>
								</head>
								<body style="width: 100% !important; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; margin: 0;padding: 0; color: #222222;font-family: \'Helvetica\', \'Arial\', sans-serif; font-weight: normal; text-align: left; line-height: 1.3;">
									<table style="border-spacing: 0; margin:0px auto; border-collapse: collapse; padding: 0; vertical-align: top; text-align: left;">
										<tr>
											<td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; padding: 0; vertical-align: top; text-align: left;" align="center" valign="top">
												<center style="width: 100%;">
														<table cellpadding="0" cellspacing="0" style="width: 100%; margin: 0 auto; text-align: inherit; font-weight:500; max-width: 580px !important;">
															<tr>
																	<td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; border-bottom: 1px solid #e5e5e5; padding: 10px; text-align: center;" >
																			<img src="'.$site_email_logo.'" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto !important;height: auto !important; max-width: 100%; clear: both;" />
																	</td>
															</tr>
															<tr>
																<td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; padding: 0; vertical-align: top; text-align: left; border-bottom: 1px solid #e5e5e5; padding: 10px; text-align: center; padding: 45px; color: #676767; font-size: 14px; line-height: 20px;">
																	<p style="color: #676767; font-size: 16px; line-height: 25px; padding-bottom: 15px; margin:0px; font-weight:bold;">
																		'.$subscriber['subscriber_name'].', there is a new blog post on
																		 <a href="'.$site_url.'" style="color: #0d4fa0;text-decoration: none;">
																		 '.$site_name.'
																		 </a>
																		 </p>
																 <p style="margin:0px;">
																		<img src="' . $image[0] . '" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: auto !important; height: auto !important; max-width: 100%; clear: both;" />
																 </p>
																 <p style="color: #676767;font-size: 16px; margin:0px;line-height: 25px; font-weight:bold; padding:10px 0px; color:#181818;">
																		Welcome to the '.$site_name.' Blog!
																 </p>
																 <br>
																 <p style="padding: 15px 0px; margin:0px; text-align:center;">
																		<a href="'.$site_url.'/blog/'.$post_url.'" class="confirm_follow" style="background: #0061aa;padding: 10px 0px;display: inline-block;color: #fff;font-size: 16px;text-decoration: none;width: 120px; text-align: center;">
																			VIEW POST
																		</a>
																 </p>
																</td>
															</tr>
															<tr>
																	<td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto;text-align:center; padding:10px 0px; border-collapse: collapse !important; padding: 0; vertical-align: top; text-align: left;">
																		<p style="color: #676767; margin:0px; font-size: 12px;line-height: 25px; text-align:center; ">
																			&#169; '. $site_name .'
																		</p>
																		<p style="color: #676767; margin:0px; font-size: 12px;line-height: 25px; text-align:center; ">
																			'.$site_address.'
																		</p>
																		<p style="color: #676767; margin:0px; font-size: 12px;line-height: 25px; text-align:center; ">
																			<a href="'.$site_url.'">'.$site_name.'</a>
																		</p>
																		<p style="color: #676767; margin:0px; font-size: 12px;line-height: 25px; text-align:center; ">
																			You are receiving this email because you opted in on our website. If you no longer want to receive these updates, you may
																			<a href="'.$site_url.'/unsubscribe/?email='.$subscriber['subscriber_email'].'">unsubscribe</a>.
																		</p>
																	</td>
															</tr>
														</table>
												</center>
											</td>
										</tr>
									</table>
								</body>
							</html>
						';
					}
						$headers = 'From: '.$site_name.' <'.$from_email_address.'>';
						wp_mail( $subscriber['subscriber_email'], $subject, $message, $headers );
			}
    }
}
