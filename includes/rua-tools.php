<?php
/**
 * RUA Tools Page
 *
*/

function create_rua_tools_page() {
	
  if('POST' == $_SERVER['REQUEST_METHOD'])
  { // start POST
    $site_id = get_current_blog_id();
    $subscriber_type = $_POST['subscriber_type'];
    $export_type = $_POST['export_type'];
    if( $export_type == 'csv')
    { // start csv
      $subscriber_type = $_POST['subscriber_type'];
      global $wpdb;
      if ( $subscriber_type == 'all' ){
      $subscriber_type_results = $wpdb->get_results( "SELECT * FROM wp_rua_blog_subscriber WHERE site_id = '$site_id'", ARRAY_A);
      }
      else
      {
         $subscriber_type_results = $wpdb->get_results( "SELECT * FROM wp_rua_blog_subscriber WHERE subscriber_status = '$subscriber_type' AND site_id = '$site_id'", ARRAY_A );
      }
      $sitename = sanitize_key( get_bloginfo( 'name' ) );
      if ( ! empty($sitename) ) $sitename .= '.';
      $filename = $sitename . '_subscriber_export_.' . $subscriber_type . '_' . date( 'Y-m-d' ) . '.csv';
      header("Pragma: public");
    	header("Expires: 0");
    	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    	header("Cache-Control: private", false);
    	header("Content-Type: application/octet-stream");
    	header( 'Content-Disposition: attachment; filename=' . $filename );
    	header("Content-Transfer-Encoding: binary");
			ob_end_clean();
      $tablefields = $wpdb->get_results( 'SHOW COLUMNS FROM wp_rua_blog_subscriber' );
      $columns = count( $tablefields );
      $field_array = array();
      for ( $i = 0; $i < $columns; $i++ )
      {
        $fieldname = $tablefields[$i]->Field;
        $field_array[] = $fieldname;
      }
      for( $i = 0; $i < $columns; $i++ )
      {
        echo $field_array[$i].",";
      }
      echo "\n";
      foreach( $subscriber_type_results as $subscriber_type_result )
      {
        foreach( $subscriber_type_result as $result ) {
          echo $result.",";
        }
        echo "\n";
      }
      exit();
    } // end csv
    else if ( $export_type == 'excel' )
    { // start excel
      //$subscriber_type = $_POST['subscriber_type'];
      global $wpdb;
      if ( $subscriber_type == 'all' )
      {
        $subscriber_type_results = $wpdb->get_results( "SELECT * FROM wp_rua_blog_subscriber WHERE site_id = '$site_id'", ARRAY_A);
      }
      else
      {
        $subscriber_type_results = $wpdb->get_results( "SELECT * FROM wp_rua_blog_subscriber WHERE subscriber_status = '$subscriber_type' AND site_id = '$site_id'", ARRAY_A );
      }

      $sitename = sanitize_key( get_bloginfo( 'name' ) );
      if ( ! empty($sitename) ) $sitename .= '.';
      $filename = $sitename . '_subscriber_export_.' . $subscriber_type . '_' . date( 'Y-m-d' ) . '.xls';
      header( 'Content-Description: File Transfer' );
      header( 'Content-Disposition: attachment; filename=' . $filename );
      header( 'Content-type: application/vnd.ms-excel; name=excel;' );
      header( 'Content-type: application/x-ms-excel;' );
      ob_end_clean();
      $tablefields = $wpdb->get_results( 'SHOW COLUMNS FROM wp_rua_blog_subscriber' );
      $columns = count( $tablefields );
      $field_array = array();
      for ( $i = 0; $i < $columns; $i++ )
      {
        $fieldname = $tablefields[$i]->Field;
        $field_array[] = $fieldname;
      }
      for( $i = 0; $i < $columns; $i++ )
      {
        echo $field_array[$i];
        print "\t";
      }
      print "\n";
      
      foreach( $subscriber_type_results as $subscriber_type_result )
      {
        foreach($subscriber_type_result as $result) {
          echo $result;
          print "\t";
        }
        print "\n";
      }
      exit();
    } // end excel
  } // end POST
  ?>
    <div class="wrap"><!-- start wrap -->
      <div class="container rua-container"><!-- start container -->
      <h3 class="text-center">Tools Page</h3>
      <hr/>
        <div class="row">
          <form id="rua_export_form" method="post" class="form-inline" action="">
          <div class="col-md-4">
                <div class="form-group">
                  <label>Type of List</label>
                  <select id="subscriber_type" name="subscriber_type" class="form-control">
                    <option value='all'>All</option>
                    <option value='subscribed'>Subscribed</option>
                    <option value='unsubscribed'>Unsubscribed</option>
                    <option value='unverified'>Unverified</option>
                  </select>
                </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                <label>Type of Export</label>
                  <select id="export_type" name="export_type" class="form-control">
                    <option value='csv'>CSV</option>
										<option value='excel'>Excel</option>
                  </select>
              </div>
          </div>
          <div class="col-md-4">
            <label>Run Export</label>
            <input type="submit" id="exportBtn" name="exportBtn" class="btn btn-success btn-sm" value="Export" />
						<i class="fa fa-download fa-lg" aria-hidden="true"></i>
          </div>
          </form>
        </div>
        <hr>
        <h3 class="text-center"><i class="fa fa-tachometer" aria-hidden="true"></i> System Info</h3>
        <div class="row">
          <div class="col-md-6">
              <h5>RUA INFO</h5>
								<p><strong>Site ID:</strong>
                <?php
                echo get_current_blog_id();
                ?>
                </p>
                <p><strong>Site URL:</strong>
                <?php
                echo get_site_url();
                ?>
                </p>
                <p><strong>Home URL:</strong>
                <?php
                echo get_home_url();
                ?>
                </p>
                <p><strong>Blog URL:</strong>
                <?php
                  echo get_option( 'rua_blog_url' );
                ?>
                </p>
                <p><strong>Contact Form URL:</strong>
                <?php
                  echo get_option( 'rua_site_contact_form' );
                ?>
                </p>
          </div>
          <div class="col-md-6">
              <h5>SERVER INFO</h5>
              <p><strong>PHP Version:</strong>
              <?php
                echo phpversion();
              ?>
              </p>
              <p><strong>Output buffering:</strong>
              <?php
              $is_output_buffering_on = ini_get('output_buffering');
              if( $is_output_buffering_on == '1' )
              {
                  echo 'ON';
              }
              else
              {
                  echo 'OFF';
              }
              ?>
              </p>
              <p><strong>Server Name:</strong>
              <?php
                echo $_SERVER['SERVER_NAME'];
              ?>
              </p>
               <p><strong>Server Version:</strong>
              <?php
                $rua_server_software = explode( ' ', trim( $_SERVER['SERVER_SOFTWARE'] ) );
                echo $rua_server_software[0];
              ?>
              </p>
          </div>
          
        </div>
        <hr>
        <div class="row">
          <div class="col-md-6">
            <div class="col-md-4">
              <h5>WORDPRESS INFO</h5>
              <p>
              <strong>WordPress Version:</strong>
              <?php
                global $wp_version;
                $rua_wp_version = $wp_version;
                echo $rua_wp_version;
              ?>
              </p>
              <p>
              <strong>Is WordPress Multisite:</strong>
              <?php
                if ( is_multisite() )
                {
                  echo 'Yes';
                }
                else
                {
                  echo 'No';
                }
              ?>
              </p>
              <p>
              <strong>WordPress Theme:</strong>
              <?php
               echo wp_get_theme();
              ?>
              </p>
              <p>
              <strong>WordPress Language:</strong>
              <?php
               echo get_locale();
              ?>
              </p>
              <p>
              <strong>ABSPATH:</strong>
              <?php
                echo ABSPATH;
              ?>
              </p>
              <p>
              <strong>WP_DEBUG:</strong>
              <?php
                $rua_wp_debug = WP_DEBUG;
                if( $rua_wp_debug == '1' )
                {
                  echo 'Enabled';
                }
                else
                {
                  echo 'Disabled';
                }
              ?>
              </p>
              <p>
              <strong>Active Plugins:</strong><br>
              <?php
                $rua_plugins_array = get_option( 'active_plugins' );
                $rua_plugins_array = implode( ' ', $rua_plugins_array);
                $rua_plugins_array = preg_split( '/[\/\s,]+/', $rua_plugins_array);
                foreach ( $rua_plugins_array as $rua_active_plugin )
                {
                  if ( strpos( $rua_active_plugin, '.') === FALSE)
                  {
                    echo $rua_active_plugin;
                  }
                  else
                  {
                    echo '<br>';
                  }
                  
                }
              ?>
              </p>
            </div>
          </div>
          <div class="col-md-6">
            <h5>USER BROWSER INFO</h5>
            <p>
            <strong>Browser Name:</strong>
            <?php
            if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== FALSE)
              echo 'Internet explorer';
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== FALSE) //For Supporting IE 11 or Edge
              echo 'Internet explorer';
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox') !== FALSE)
              echo 'Mozilla Firefox';
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome') !== FALSE)
              echo 'Google Chrome';
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Opera') !== FALSE)
              echo "Opera";
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Safari') !== FALSE)
              echo "Safari";
            else
              echo 'Something else';
            ?>
            </p>
            
            <p>
            <strong>User Platform:</strong>
            <?php
            if(strpos($_SERVER['HTTP_USER_AGENT'], 'Macintosh') !== FALSE)
              echo 'Apple Mac';
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Windows') !== FALSE) //For Supporting IE 11 or Edge
              echo 'Windows';
            elseif(strpos($_SERVER['HTTP_USER_AGENT'], 'Linux') !== FALSE)
              echo 'Linux';
            else
              echo 'Something else';
            ?>
            </p>
            <p>
            <strong>User Agent String:</strong>
            <?php
              echo $_SERVER['HTTP_USER_AGENT'];
            ?>
            </p>
          </div>
        </div>
      </div><!-- end container -->
    </div><!-- end wrap -->
    <?php
}
