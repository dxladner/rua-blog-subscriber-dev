<?php
/**
 * RUA Subscriber Form Page
 *
*/

function rua_show_subscriber_form( $atts, $content = null ) {

	session_start();
	$_SESSION = array();
	$_SESSION['captcha'] = simple_php_captcha();

	$site_id = get_current_blog_id();
	if( $site_id == 0)
	{
		$site_id = 1;
	}
	else
	{
		$site_id = get_current_blog_id();
	}
	global $wpdb;
  $wpdb->get_results( "SELECT * FROM wp_rua_blog_subscriber WHERE subscriber_status = 'subscribed' AND site_id = '$site_id'" );
  $subscriber_count = $wpdb->num_rows;
	$subscribe_date = date('Y-m-d');
	$rua_form_header = get_option('rua_form_header');
	$rua_form_message = get_option('rua_form_message');
	$rua_custom_message = get_option('rua_custom_message');
	$rua_button_text = get_option('rua_button_text');
	$rua_button_color = get_option('rua_button_color');
	$rua_form_header_size = get_option('rua_form_header_size');
	$rua_show_form_subscribers = get_option('rua_show_form_subscribers');
	$rua_turn_on_captcha = get_option( 'rua_turn_on_captcha' );

	$a = shortcode_atts( array(
      "site_id" => $site_id,
			"subscribe_date" => $subscribe_date,
		  "rua_form_header" => $rua_form_header,
		  "rua_form_message" => $rua_form_message,
		  "rua_custom_message" => $rua_custom_message,
			"rua_button_text" => $rua_button_text,
		  "rua_button_color" => $rua_button_color,
		  "rua_form_header_size" => $rua_form_header_size,
			"subscriber_count"			=> $subscriber_count,
    ), $atts );

	$content .= '<div id="formwrapper">';
	$content .= '<'.esc_attr( $a['rua_form_header_size'] ).' class="subscribe_label">'. esc_attr( $a['rua_form_header'] ) .'</'.esc_attr( $a['rua_form_header_size'] ).'>';
  $content .= '<div id="subscribe_form">
        <p>'. esc_attr( $a['rua_form_message'] ) .'</p>
				<div class="iconspinner"><i class="fa fa-spinner fa-3x fa-spin-custom"></i></div>';
	if($rua_show_form_subscribers == 'on')
	{
		$content .=	'<div class="numofsubscribers"><p>Join '. esc_attr( $a['subscriber_count'] ) .' other subscribers.</p></div>';
	}
	$content .= '<form id="subscribeform" novalidate="novalidate">
          <div>
            <label for="ruaname">NAME <span>*</span></label>
              <div>
                <input type="text" id="ruaname" name="ruaname" />
              </div>
          </div>
          <div>
            <label for="ruaemail">EMAIL <span>*</span></label>
              <div>
                <input type="email" id="ruaemail" name="ruaemail" />
              </div>
          </div>';
					if($rua_turn_on_captcha == 'on')
					{
						$content .= '<div>
	            <label for="phpcaptcha">Captcha <span>*</span></label>
	              <div>
	                <input type="text" id="phpcaptcha" name="phpcaptcha" />
	              </div>
	          </div>';
					}
          $content .= '<div>
					  '. wp_nonce_field( 'rua_blog_subscriber', 'rua_blog_subscriber_nonce' ) .'
            <input type="hidden" id="ruasubstatus" name="ruasubstatus" value="unverified" />
            <input type="hidden" id="ruasiteid" name="ruasiteid" value="' . esc_attr( $a['site_id'] ) . '" />
            <input type="hidden" id="ruasubdate" name="ruasubdate" value="' . esc_attr( $a['subscribe_date'] ) . '" />
            <input type="submit" class="subscribe_btn" id="ruaSubmit" name="ruaSubmit" value="'. esc_attr( $a['rua_button_text'] ) .'" /><br>
            <span id="ruaValidationEmailMesage"></span>
          </div>
					<br />';
					if($rua_turn_on_captcha == 'on')
					{
						$content .= '<img class="captcha-image" src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA" /><br />
						<span style="color: red;" id="ruaCaptchaFailureMesage">Your Capthca Failed. Page will refresh in a second. Please try again.</span>';
					}
				$content .= '</form>
      </div>';
			?>
      <style>
				#successMessage {
					background: #FBFBFB url("<?php echo RUA_PLUGIN_URL . "images/email.png"; ?>") no-repeat 50% bottom;
				}
				#subscribe_form {
						background-color: <?php echo get_option('rua_background_color'); ?> !important;
				}
				#subscribe_form p,label {
					color: <?php echo get_option('rua_form_text_color'); ?> !important;
				}
				#ruaSubmit {
					color: <?php echo get_option('rua_button_text_color'); ?> !important;
					background-color: <?php echo get_option('rua_button_color'); ?> !important;
				}
      </style>
      <?php
      $content .= '<div id="successMessage" name="successMessage">
					<p class="success_description">'. esc_attr( $a['rua_custom_message'] ) .'
          </p>
					<i class="fa fa-check-circle-o fa-3x"></i>
      <p class="subscribe-text-green">Success!</p>
					</div>';
      $content .= '</div>';
	return $content;
}
add_shortcode('ruasubscriber', 'rua_show_subscriber_form');
