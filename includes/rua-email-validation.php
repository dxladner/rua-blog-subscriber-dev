<?php
/**
 * RUA Email Validation Page
 *
*/

if ( !function_exists( 'rua_email_validation' ) )
{
	function rua_email_validation() {
		$email = is_email( $_POST['ruaemail'] );
		$email = sanitize_email( $email );
		$site_id = get_current_blog_id();
		global $wpdb;

		$wpdb->get_results( "SELECT subscriber_email FROM wp_rua_blog_subscriber WHERE subscriber_email = '$email' AND site_id = '$site_id'" );
		$is_in_database = $wpdb->num_rows;
		if ($is_in_database >= 1)
		{
			 echo '1'; // Record match in DB. Email already being used
		}
		else
		{
				 echo '0'; // Email is available
		}
		die();
	}
}
add_action( 'wp_ajax_rua_email_validation', 'rua_email_validation' );
add_action( 'wp_ajax_nopriv_rua_email_validation', 'rua_email_validation' );
